UID := 1000
GID := 1000

list:
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  install       > install php composer dependancies"
	@echo "  rebuild       > rebuild php image and start containers for dev only"
	@echo "  start         > start containers"
	@echo "  restart       > restart containers"
	@echo "  stop          > stop and kill running containers"
	@echo "  status        > display stack containers status"
	@echo "  logs          > display containers logs"
	@echo "  shell         > shell into php container"
	@echo "  phpunit       > run php unit test suite"
	@echo "  phpcs         > run php code sniffer test suite"
	@echo "  create-db     > create a database for dev only (need token_enabled=0)"
	@echo "  remove-pgdata > remove the metadata database"
	@echo ""

install:
	@docker run --init -it --rm --user $(UID):$(GID) \
	-e COMPOSER_CACHE_DIR=/dev/null \
	-v $(CURDIR):/project \
	-w /project jakzal/phpqa:php8.0 composer install --ignore-platform-reqs

rebuild:
	@docker-compose up --build -d

start:
	@docker-compose up -d

restart: stop start

stop:
	@docker-compose kill
	@docker-compose rm -v --force

status:
	@docker-compose ps

logs:
	@docker-compose logs -f -t

shell:
	@docker-compose exec php bash

phpunit:
	@docker run --init -it --rm --user $(UID):$(GID) \
	-v $(CURDIR):/project \
	-w /project jakzal/phpqa:php8.0 phpdbg -qrr ./vendor/bin/phpunit --bootstrap ./tests/bootstrap.php \
	--whitelist src --colors --coverage-html ./phpunit-coverage ./tests

phpcs:
	@docker run --init -it --rm --user $(UID):$(GID) \
	-v $(CURDIR):/project \
	-w /project jakzal/phpqa:php8.0 phpcs --standard=PSR12 --extensions=php --colors src tests

create-db:
	@docker-compose exec php sh ./conf-dev/init-keycloak.sh
	@docker-compose exec php sh ./conf-dev/create-db.sh

remove-pgdata:
	@docker volume rm anis-server_pgdata
