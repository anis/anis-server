<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

use Slim\Routing\RouteCollectorProxy;

$app->get('/', App\Action\RootAction::class);

$app->group('', function (RouteCollectorProxy $group) {
    $group->map([OPTIONS, GET, POST], '/select', App\Action\SelectListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/select/{name}', App\Action\SelectAction::class);
    $group->map([OPTIONS, GET, POST], '/option', App\Action\OptionListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/option/{id}', App\Action\OptionAction::class);
    $group->map([OPTIONS, GET, POST], '/database', App\Action\DatabaseListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/database/{id}', App\Action\DatabaseAction::class);
    $group->map([OPTIONS, GET], '/database/{id}/table', App\Action\TableListAction::class);
    $group->map([OPTIONS, GET], '/file-explorer[{fpath:.*}]', App\Action\AdminFileExplorerAction::class);
})->add(new App\Middleware\RouteGuardMiddleware(
    boolval($container->get(SETTINGS)['token']['enabled']), 
    array(GET, POST, PUT, DELETE), 
    $container->get(SETTINGS)['token']['admin_role']
));

$app->group('', function (RouteCollectorProxy $group) {
    $group->map([OPTIONS, GET, POST], '/survey', App\Action\SurveyListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/survey/{name}', App\Action\SurveyAction::class);
    $group->map([OPTIONS, GET, POST], '/instance', App\Action\InstanceListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}', App\Action\InstanceAction::class);
    $group->map([OPTIONS, GET, POST], '/instance/{name}/group', App\Action\GroupListAction::class);
    $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset-family', App\Action\DatasetFamilyListAction::class);
    $group->map([OPTIONS, GET], '/instance/{name}/dataset', App\Action\DatasetListByInstanceAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/group/{id}', App\Action\GroupAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/dataset-family/{id}', App\Action\DatasetFamilyAction::class);
    $group->map([OPTIONS, GET, POST], '/dataset-family/{id}/dataset', App\Action\DatasetListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/dataset/{name}', App\Action\DatasetAction::class);
    $group->map([OPTIONS, GET, POST], '/dataset/{name}/criteria-family', App\Action\CriteriaFamilyListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/criteria-family/{id}', App\Action\CriteriaFamilyAction::class);
    $group->map([OPTIONS, GET, POST], '/dataset/{name}/output-family', App\Action\OutputFamilyListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/output-family/{id}', App\Action\OutputFamilyAction::class);
    $group->map([OPTIONS, GET], '/dataset/{name}/output-category', App\Action\OutputCategoryListByDatasetAction::class);
    $group->map(
        [OPTIONS, GET, POST],
        '/output-family/{id}/output-category',
        App\Action\OutputCategoryListAction::class
    );
    $group->map([OPTIONS, GET, PUT, DELETE], '/output-category/{id}', App\Action\OutputCategoryAction::class);
    $group->map([OPTIONS, GET, POST], '/dataset/{name}/attribute', App\Action\AttributeListAction::class);
    $group->map([OPTIONS, GET], '/dataset/{name}/column', App\Action\ColumnListAction::class);
    $group->map([OPTIONS, GET, PUT, DELETE], '/dataset/{name}/attribute/{id}', App\Action\AttributeAction::class);
    $group->map(
        [OPTIONS, GET, PUT],
        '/dataset/{name}/attribute/{id}/distinct',
        App\Action\AttributeDistinctAction::class
    );
})->add(new App\Middleware\RouteGuardMiddleware(
    boolval($container->get(SETTINGS)['token']['enabled']), 
    array(POST, PUT, DELETE), 
    $container->get(SETTINGS)['token']['admin_role']
));

$app->get('/search/{dname}', App\Action\SearchAction::class);
$app->get('/archive/{dname}', App\Action\ArchiveAction::class);
$app->get('/dataset-file-explorer/{dname}[{fpath:.*}]', App\Action\DatasetFileExplorerAction::class);
$app->get('/download-file/{dname}/[{fpath:.*}]', App\Action\DownloadFileAction::class);
