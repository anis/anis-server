<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

return [
    'displayErrorDetails' => getenv('DISPLAY_ERROR_DETAILS'),
    'database' => [
        'path_proxy' => getenv('DATABASE_PATH_PROXY'),
        'dev_mode' => getenv('DATABASE_DEV_MODE'),
        'connection_options' => [
            'driver' => getenv('DATABASE_CO_DRIVER'),
            'path' => getenv('DATABASE_CO_PATH'),
            'host' => getenv('DATABASE_CO_HOST'),
            'port' => (int) getenv('DATABASE_CO_PORT'),
            'dbname' => getenv('DATABASE_CO_DBNAME'),
            'user' => getenv('DATABASE_CO_USER'),
            'password' => getenv('DATABASE_CO_PASSWORD')
        ],
    ],
    'data_path' => getenv('DATA_PATH'),
    'logger' => [
        'name' => getenv('LOGGER_NAME'),
        'path' => getenv('LOGGER_PATH'),
        'level' => getenv('LOGGER_LEVEL')
    ],
    'token' => [
        //'issuer' => getenv('TOKEN_ISSUER'),
        'enabled' => getenv('TOKEN_ENABLED'),
        'public_key_file' => getenv('TOKEN_PUBLIC_KEY_FILE'),
        'admin_role' => getenv('TOKEN_ADMIN_ROLE')
    ]
];
