FROM php:8.0.3-apache

# Install modules
RUN apt-get update \ 
    && apt-get install -y zlib1g zlib1g-dev libpq-dev libpq5 libzip-dev zip unzip \
    && docker-php-ext-install pgsql pdo_pgsql zip bcmath

RUN printf "\n" | pecl install apcu

RUN a2enmod rewrite

COPY . /project
COPY ./conf-dev/vhost.conf /etc/apache2/sites-available/000-default.conf
COPY ./conf-dev/php.ini /usr/local/etc/php/conf.d/app.ini

WORKDIR /project

CMD ["apache2-foreground"]
