<?php
// File needed by doctrine cli
require 'vendor/autoload.php';

$settings = require './app/settings.php';
$database = $settings['database'];
$devMode = boolval($database['dev_mode']);
$proxyDir = getcwd() . '/doctrine-proxy';
$cache = null;
if ($devMode == false) {
    $cache = new \Doctrine\Common\Cache\ApcuCache();
}

$c = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
    array('src/Entity'),
    $devMode,
    $proxyDir,
    $cache
);
$c->setAutoGenerateProxyClasses(false);

if ($devMode) {
    $c->setSQLLogger(new \Doctrine\DBAL\Logging\DebugStack());
} else {
    $c->setQueryCacheImpl($cache);
}

$em = \Doctrine\ORM\EntityManager::create($database['connection_options'], $c);

$helpers = new Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
));

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($em);
