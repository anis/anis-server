# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.6.0]
 - In progress...

### Added
 - #73: Create route for retrieve archive (all files for a search)
 - #70: Add endpoint to add or delete an attribute
 - #69: Add endpoint to list all columns for a table
 - #66: Export all attributes (a=all)
 - #48: Export to votable format
 - #64: Data management system

### Changed
 - #72: Change entity name project by survey
 - #71: Update metamodel database
 - #68: Deleting property table_name from attribute entity
 - #67: Body Parsing and Content Length slim's middlewares
 - #61: Docker images are stored on the gitlab internal registry
 - #63: Update composer.json dependencies and using php version 8
 - #65: Delete cascade (instance, dataset, group, settings_select)

## [3.5.0]
### Added
 - #7: Protected routes with SSO JWT
 - #58: Add datasets privileges

## [3.4.0]
### Added
- #56: Added config attribute to Instance entity
 
### Changed
- #56: Remove 'selectable_row' from Dataset entity
 
 ## [3.3.0]
### Added
- #54: Added search by cone-search

## [3.2.1]
### Fixed
- #55: Bug json in json when only one column selected

## [3.2.0]
### Added
- #42: The GET method of the InstanceListAction returns the list of available instances with **the number of dataset families** and the **number of available datasets** for each instance
- #45: New route which allows to return the distinct values ​​of an attribute for one dataset used to generate the criterion option list => **/dataset/{name}/attribute/{id}/distinct**
- #46: Adding param "f" to export result in different format (json, csv, ascii) for the SearchAction
- #50: Middleware to log metamodel sql requests added (only for the dev mode)
- #51: Adding an action to download files associated to a dataset

### Changed
- #44: Adding the doctrine production mode (see readme)
- #52: Changing metamodel attribute table (uri_action field deleted and renderer_config field added)
- #53: Changing metamodel dataset table (add config field)

### Deprecated
- For soon-to-be removed features. 

### Removed
- For now removed features.

### Fixed
 - #41: The client_url field must be optional in the InstanceListAction when the user adding a new instance in the metamodel
 - #43: The json operator works with PostgreSQL databases
 - #49: LIKE and NOT LIKE didn't work with an attribut type numeric

### Security
- In case of vulnerabilities. 