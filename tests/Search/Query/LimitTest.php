<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query;

use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Search\Query\Limit;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Dataset;
use App\Search\Query\SearchQueryException;

final class LimitTest extends TestCase
{
    public function testLimit(): void
    {
        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $doctrineQueryBuilder->method('setFirstResult')->willReturn($doctrineQueryBuilder);
        $doctrineQueryBuilder->expects($this->once())->method('setFirstResult');
        $doctrineQueryBuilder->expects($this->once())->method('setMaxResults');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $datasetSelected = $this->createMock(Dataset::class);
        (new Limit())($anisQueryBuilder, $datasetSelected, array('p' => '1:10'));
    }

    public function testLimitException(): void
    {
        $this->expectException(SearchQueryException::class);
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $datasetSelected = $this->createMock(Dataset::class);
        (new Limit())($anisQueryBuilder, $datasetSelected, array('p' => '1:10:20'));
    }
}
