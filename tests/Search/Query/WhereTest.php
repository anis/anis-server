<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query;

use PHPUnit\Framework\TestCase;
use App\Search\Query\Where;
use App\Search\Query\AnisQueryBuilder;
use App\Search\Query\Operator\IOperatorFactory;
use App\Search\Query\Operator\IOperator;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use App\Entity\Dataset;
use App\Entity\Attribute;

final class WhereTest extends TestCase
{
    public function testWhere(): void
    {
        $operatorFactory = $this->createMock(IOperatorFactory::class);
        $operator = $this->createMock(IOperator::class);
        $operatorFactory->method('create')->willReturn($operator);

        $id = $this->createMock(Attribute::class);
        $id->method('getId')->willReturn(1);
        $id->method('getType')->willReturn('integer');
        $datasetSelected = $this->createMock(Dataset::class);
        $datasetSelected->method('getAttributes')->willReturn(array($id));

        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $expr = $this->createMock(ExpressionBuilder::class);
        $doctrineQueryBuilder->method('expr')->willReturn($expr);
        $doctrineQueryBuilder->expects($this->once())->method('andWhere');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = array('c' => '1::eq::10');
        (new Where($operatorFactory))($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    public function testWhereWithoutValues(): void
    {
        $operatorFactory = $this->createMock(IOperatorFactory::class);
        $operator = $this->createMock(IOperator::class);
        $operatorFactory->method('create')->willReturn($operator);

        $id = $this->createMock(Attribute::class);
        $id->method('getId')->willReturn(1);
        $id->method('getType')->willReturn('integer');
        $datasetSelected = $this->createMock(Dataset::class);
        $datasetSelected->method('getAttributes')->willReturn(array($id));

        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $expr = $this->createMock(ExpressionBuilder::class);
        $doctrineQueryBuilder->method('expr')->willReturn($expr);
        $doctrineQueryBuilder->expects($this->once())->method('andWhere');
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = array('c' => '1::nl');
        (new Where($operatorFactory))($anisQueryBuilder, $datasetSelected, $queryParams);
    }
}
