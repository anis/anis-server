<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query\Operator;

use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use App\Search\Query\Operator\OperatorFactory;
use App\Search\Query\Operator\OperatorException;

final class OperatorFactoryTest extends TestCase
{
    /**
     * @var OperatorFactory
     */
    private $operatorFactory;

    protected function setUp(): void
    {
        $this->operatorFactory = new OperatorFactory();
    }

    public function testUnknownOperatorException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'un',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20')
        );
    }

    public function testTypeCompatibilityException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'eq',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('aaa')
        );
    }

    public function testCreateBetween(): void
    {
        $between = $this->operatorFactory->create(
            'bw',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\Between', $between);
    }

    public function testCreateBetweenException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'bw',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20', '30')
        );
    }

    public function testCreateEqual(): void
    {
        $equal = $this->operatorFactory->create(
            'eq',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\Equal', $equal);
    }

    public function testCreateException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'eq',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20')
        );
    }

    public function testCreateNotEqual(): void
    {
        $notEqual = $this->operatorFactory->create(
            'neq',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\NotEqual', $notEqual);
    }

    public function testCreateNotEqualException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'neq',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20')
        );
    }

    public function testCreateGreaterThan(): void
    {
        $greaterThan = $this->operatorFactory->create(
            'gt',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\GreaterThan', $greaterThan);
    }

    public function testCreateGreaterThanException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'gt',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20')
        );
    }

    public function testCreateGreaterThanEqual(): void
    {
        $greaterThanEqual = $this->operatorFactory->create(
            'gte',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\GreaterThanEqual', $greaterThanEqual);
    }

    public function testCreateGreaterThanEqualException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'gte',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20')
        );
    }

    public function testCreateLessThan(): void
    {
        $lessThan = $this->operatorFactory->create(
            'lt',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\LessThan', $lessThan);
    }

    public function testCreateLessThanException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'lt',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20')
        );
    }

    public function testCreateLessThanEqual(): void
    {
        $lessThanEqual = $this->operatorFactory->create(
            'lte',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\LessThanEqual', $lessThanEqual);
    }

    public function testCreateLessThanEqualException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'lte',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20')
        );
    }

    public function testCreateLike(): void
    {
        $like = $this->operatorFactory->create(
            'lk',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\Like', $like);
    }

    public function testCreateLikeException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'lk',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20')
        );
    }

    public function testCreateNotLike(): void
    {
        $notLike = $this->operatorFactory->create(
            'nlk',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\NotLike', $notLike);
    }

    public function testCreateNotLikeException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'nlk',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20')
        );
    }

    public function testCreateIn(): void
    {
        $in = $this->operatorFactory->create(
            'in',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20', '30')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\In', $in);
    }

    public function testCreateInException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'in',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array()
        );
    }

    public function testCreateNotIn(): void
    {
        $notIn = $this->operatorFactory->create(
            'nin',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10', '20', '30')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\NotIn', $notIn);
    }

    public function testCreateNotInException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'nin',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array()
        );
    }

    public function testCreateNull(): void
    {
        $null = $this->operatorFactory->create(
            'nl',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array()
        );
        $this->assertInstanceOf('App\Search\Query\Operator\OperatorNull', $null);
    }

    public function testCreateNullException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'nl',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10')
        );
    }

    public function testCreateNotNull(): void
    {
        $notNull = $this->operatorFactory->create(
            'nnl',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array()
        );
        $this->assertInstanceOf('App\Search\Query\Operator\OperatorNotNull', $notNull);
    }

    public function testCreateNotNullException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'nnl',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('10')
        );
    }

    public function testcreateJsonPostgres(): void
    {
        $json = $this->operatorFactory->create(
            'js',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'json',
            array('path', 'eq', '10')
        );
        $this->assertInstanceOf('App\Search\Query\Operator\JsonPostgres', $json);
    }

    public function testcreateJsonPostgresException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'js',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'json',
            array('path', 'eq')
        );
    }

    public function testcreateJsonPostgresTypeException(): void
    {
        $this->expectException(OperatorException::class);
        $this->operatorFactory->create(
            'js',
            $this->createMock(ExpressionBuilder::class),
            'test',
            'integer',
            array('path', 'eq', '10')
        );
    }
}
