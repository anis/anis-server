<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query\Operator;

use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use App\Search\Query\Operator\GreaterThanEqual;

final class GreaterThanEqualTest extends TestCase
{
    private $operator;

    protected function setUp(): void
    {
        $expr = $this->createMock(ExpressionBuilder::class);
        $expr->method('gte')
            ->willReturn('test >= 10');
        $this->operator = new GreaterThanEqual($expr, 'test', 'integer', '10');
    }

    public function testGetExpression(): void
    {
        $this->assertSame('test >= 10', $this->operator->getExpression());
    }
}
