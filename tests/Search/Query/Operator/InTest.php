<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Query\Operator;

use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use App\Search\Query\Operator\In;

final class InTest extends TestCase
{
    private $operator;

    protected function setUp(): void
    {
        $expr = $this->createMock(ExpressionBuilder::class);
        $expr->method('in')
            ->willReturn('test IN(10, 20, 30)');
        $this->operator = new In($expr, 'test', 'integer', array('10', '20', '30'));
    }

    public function testGetExpression(): void
    {
        $this->assertSame('test IN(10, 20, 30)', $this->operator->getExpression());
    }
}
