<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Response;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\Response;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Search\Response\TextResponse;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Attribute;

final class TextResponseTest extends TestCase
{
    public function testGetResponse(): void
    {
        $stmt = $this->createMock(Statement::class);
        $stmt->method('fetch')->willReturnOnConsecutiveCalls(array(
            'id' => 1,
            'ra' => 102.5,
            'dec' => 0.1
        ), false);

        $id = $this->createMock(Attribute::class);
        $id->method('getLabel')->willReturn('id');
        $ra = $this->createMock(Attribute::class);
        $ra->method('getLabel')->willReturn('ra');
        $dec = $this->createMock(Attribute::class);
        $dec->method('getLabel')->willReturn('dec');

        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $doctrineQueryBuilder->method('execute')->willReturn($stmt);
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $anisQueryBuilder->method('getAttributesSelected')->willReturn(array($id, $ra, $dec));

        $textResponse = new TextResponse(',', 'text/csv');
        $response = $textResponse->getResponse(new Response(), $anisQueryBuilder);
        $lines = explode(PHP_EOL, (string) $response->getBody());
        $this->assertSame('text/csv', $response->getHeaders()['Content-Type'][0]);
        $this->assertSame('id,ra,dec', $lines[0]);
        $this->assertSame('1,102.5,0.1', $lines[1]);
    }
}
