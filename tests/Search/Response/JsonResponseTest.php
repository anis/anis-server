<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Response;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\Response;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Search\Response\JsonResponse;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Attribute;

final class JsonResponseTest extends TestCase
{
    public function testGetResponse(): void
    {
        $stmt = $this->createMock(Statement::class);
        $stmt->method('fetch')->willReturnOnConsecutiveCalls(array(
            'id' => 1,
            'ra' => 102.5,
            'dec' => 0.1,
            'json' => '{"mode":"GP","name":"MXT-EVT-CAL"}'
        ), false);

        $id = $this->createMock(Attribute::class);
        $id->method('getLabel')->willReturn('id');
        $ra = $this->createMock(Attribute::class);
        $ra->method('getLabel')->willReturn('ra');
        $dec = $this->createMock(Attribute::class);
        $dec->method('getLabel')->willReturn('dec');
        $dec = $this->createMock(Attribute::class);
        $dec->method('getLabel')->willReturn('json');
        $dec->method('getType')->willReturn('json');

        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $doctrineQueryBuilder->method('execute')->willReturn($stmt);
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $anisQueryBuilder->method('getAttributesSelected')->willReturn(array($id, $ra, $dec));

        $textResponse = new JsonResponse();
        $r = new Response(200, array(
            'Content-Type' => 'application/json'
        ));
        $response = $textResponse->getResponse(
            $r,
            $anisQueryBuilder
        );
        $this->assertSame('application/json', $response->getHeaders()['Content-Type'][0]);
        $this->assertSame(
            '[{"id":1,"ra":102.5,"dec":0.1,"json":{"mode":"GP","name":"MXT-EVT-CAL"}}]',
            (string) $response->getBody()
        );
    }
}
