<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Search\Response;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\Response;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Search\Response\VotableResponse;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Attribute;

final class VotableResponseTest extends TestCase
{
    public function testGetResponse(): void
    {
        $stmt = $this->createMock(Statement::class);
        $stmt->method('fetch')->willReturnOnConsecutiveCalls(array(
            'id' => 1,
            'ra' => 102.5,
            'dec' => 0.1
        ), false);

        $id = $this->createMock(Attribute::class);
        $id->method('getLabel')->willReturn('id');
        $ra = $this->createMock(Attribute::class);
        $ra->method('getLabel')->willReturn('ra');
        $ra->method('getVoUcd')->willReturn('pos.eq.ra');
        $ra->method('getVoDatatype')->willReturn('double');
        $ra->method('getVoUnit')->willReturn('deg');
        $ra->method('getVoSize')->willReturn('19*');
        $ra->method('getVoUtype')->willReturn('test_utype');
        $ra->method('getVoDescription')->willReturn('Test description');
        $dec = $this->createMock(Attribute::class);
        $dec->method('getLabel')->willReturn('dec');

        $connection = $this->createMock(Connection::class);
        $connection->method('fetchOne')->willReturn(10);

        $doctrineQueryBuilder = $this->createMock(DoctrineQueryBuilder::class);
        $doctrineQueryBuilder->method('execute')->willReturn($stmt);
        $doctrineQueryBuilder->method('getConnection')->willReturn($connection);
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $anisQueryBuilder->method('getAttributesSelected')->willReturn(array($id, $ra, $dec));

        $textResponse = new VotableResponse();
        $response = $textResponse->getResponse(new Response(), $anisQueryBuilder);
        $this->assertSame('text/xml', $response->getHeaders()['Content-Type'][0]);
    }
}
