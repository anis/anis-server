<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use App\Entity\Database;
use App\Entity\Survey;

final class SurveyListActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\SurveyListAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetAllSurveys(): void
    {
        $repository = $this->createMock(ObjectRepository::class);
        $repository->expects($this->once())->method('findAll');
        $this->entityManager->method('getRepository')->with('App\Entity\Survey')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array());
    }

    public function testAddANewSurveyEmptyNameField(): void
    {
        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param name needed to add a new survey');
        $request = $this->getRequest('POST')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array());
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewSurveyDatabaseNotFound(): void
    {
        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Database with id 1 is not found');
        $fields = $this->getNewSurveyFields();
        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array());
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewSurvey(): void
    {
        $database = $this->createMock(Database::class);
        $database->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($database);

        $this->entityManager->expects($this->once())->method('persist');

        $fields = $this->getNewSurveyFields();

        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array());
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/survey', array(
            'Content-Type' => 'application/json'
        ));
    }

    private function getNewSurveyFields(): array
    {
        return array(
            'name' => 'anis_survey',
            'label' => 'Anis survey Test',
            'description' => 'Anis survey Test',
            'link' => 'http://survey.com',
            'manager' => 'M. Durand',
            'id_database' => 1
        );
    }
}
