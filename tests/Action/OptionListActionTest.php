<?php

/*
 * This file is part of Anis Settings.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use App\Entity\Select;
use App\Entity\Option;

final class OptionListActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\OptionListAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetOptionListForOneSelect(): void
    {
        $repository = $this->createMock(ObjectRepository::class);
        $repository->expects($this->once())->method('findAll');
        $this->entityManager->method('getRepository')->with('App\Entity\Option')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array());
    }

    public function testAddANewOptionEmptyLabelField(): void
    {
        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to add a new option');
        $request = $this->getRequest('POST')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array());
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testSelectIsNotFound(): void
    {
        $fields = array(
            'label' => 'ID',
            'value' => 'ID',
            'display' => 10,
            'select_name' => 'select'
        );
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Select with name select is not found');
        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array());
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testAddANewOption(): void
    {
        $select = $this->createMock(Select::class);
        $select->method('getName')->willReturn('search_flag');
        $this->entityManager->method('find')->willReturn($select);

        $this->entityManager->expects($this->once())->method('persist');

        $fields = array(
            'label' => 'ID',
            'value' => 'ID',
            'display' => 10,
            'select_name' => 'search_flag'
        );

        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array());
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/select/search_flag/option', array(
            'Content-Type' => 'application/json'
        ));
    }
}
