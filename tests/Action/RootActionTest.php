<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;

final class RootActionTest extends TestCase
{
    private $action;

    protected function setUp(): void
    {
        $this->action = new \App\Action\RootAction();
    }

    public function testAction(): void
    {
        $request = new ServerRequest('GET', '/', array(
            'Content-Type' => 'application/json'
        ));
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame('{"message":"it works!"}', (string) $response->getBody());
    }
}
