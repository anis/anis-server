<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use App\Entity\OutputFamily;

final class OutputFamilyActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\OutputFamilyAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testOutputFamilyIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Output family with id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAnOutputFamilyById(): void
    {
        $outputFamily = $this->createMock(OutputFamily::class);
        $outputFamily->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($outputFamily);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testEditAnOutputFamilyEmptyLabelField(): void
    {
        $outputFamily = $this->createMock(OutputFamily::class);
        $this->entityManager->method('find')->willReturn($outputFamily);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to edit the output family');
        $request = $this->getRequest('PUT')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditAnOutputFamily(): void
    {
        $outputFamily = $this->createMock(OutputFamily::class);
        $this->entityManager->method('find')->willReturn($outputFamily);
        $this->entityManager->expects($this->once())->method('flush');

        $fields = array(
            'label' => 'Modfied family',
            'display' => 20
        );

        $request = $this->getRequest('PUT')->withParsedBody($fields);
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testDeleteAnOutputFamily(): void
    {
        $outputFamily = $this->createMock(OutputFamily::class);
        $outputFamily->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($outputFamily);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertSame(
            json_encode(array('message' => 'Output family with id 1 is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/output-family/1', array(
            'Content-Type' => 'application/json'
        ));
    }
}
