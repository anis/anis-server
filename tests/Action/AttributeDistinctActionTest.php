<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Driver\ResultStatement;
use App\Search\DBALConnectionFactory;
use App\Entity\Attribute;
use App\Entity\Database;
use App\Entity\Dataset;
use App\Entity\Survey;

final class AttributeDistinctActionTest extends TestCase
{
    private $action;
    private $entityManager;
    private $connectionFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->connectionFactory = $this->createMock(DBALConnectionFactory::class);
        $this->action = new \App\Action\AttributeDistinctAction(
            $this->entityManager,
            $this->connectionFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    public function testAttributeIsNotFound(): void
    {
        $repository = $this->createMock(ObjectRepository::class);
        $repository->method('findOneBy')->willReturn(null);
        $this->entityManager->method('getRepository')->willReturn($repository);

        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Attribute with dataset name obs_cat and attribute id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat', 'id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetDistinctValues(): void
    {
        $database = $this->createMock(Database::class);
        $survey = $this->createMock(Survey::class);
        $survey->method('getDatabase')->willReturn($database);
        $dataset = $this->createMock(Dataset::class);
        $dataset->method('getSurvey')->willReturn($survey);

        $attribute = $this->createMock(Attribute::class);
        $attribute->method('getDataset')->willReturn($dataset);

        $repository = $this->createMock(ObjectRepository::class);
        $repository->method('findOneBy')->willReturn($attribute);
        $this->entityManager->method('getRepository')->willReturn($repository);

        $stmt = $this->createMock(ResultStatement::class);
        $stmt->method('fetchAll')->willReturn(array('value1', 'value2'));
        $queryBuilder = $this->createMock(QueryBuilder::class);
        $queryBuilder->method('execute')->willReturn($stmt);
        $connection = $this->createMock(Connection::class);
        $connection->method('createQueryBuilder')->willReturn($queryBuilder);
        $this->connectionFactory->method('create')->willReturn($connection);

        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'obs_cat', 'id' => 1));
        $this->assertSame(
            '["value1","value2"]',
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/dataset/obs_cat/attribute/1/distinct', array(
            'Content-Type' => 'application/json'
        ));
    }
}
