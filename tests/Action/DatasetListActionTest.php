<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use App\Entity\Survey;
use App\Entity\DatasetFamily;

final class DatasetListActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\DatasetListAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testDatasetFamilyNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset family with id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAllDatasets(): void
    {
        $datasetFamily = $this->createMock(DatasetFamily::class);
        $this->entityManager->method('find')->willReturn($datasetFamily);

        $repository = $this->createMock(ObjectRepository::class);
        $repository->expects($this->once())->method('findBy')->with(
            array('datasetFamily' => $datasetFamily)
        );
        $this->entityManager->method('getRepository')->with('App\Entity\Dataset')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testAddANewDatasetEmptyNameField(): void
    {
        $datasetFamily = $this->createMock(DatasetFamily::class);
        $this->entityManager->method('find')->willReturn($datasetFamily);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param name needed to add a new dataset');
        $request = $this->getRequest('POST')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewDatasetSurveyNotFound(): void
    {
        $datasetFamily = $this->createMock(DatasetFamily::class);
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($datasetFamily, null);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Survey with name anis_survey is not found');
        $fields = $this->getNewDatasetFields();
        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewDataset(): void
    {
        $datasetFamily = $this->createMock(DatasetFamily::class);
        $survey = $this->createMock(Survey::class);
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($datasetFamily, $survey);

        $this->entityManager->expects($this->once())->method('persist');

        $fields = $this->getNewDatasetFields();
        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/instance/aspic/dataset', array(
            'Content-Type' => 'application/json'
        ));
    }

    private function getNewDatasetFields(): array
    {
        return array(
            'name' => 'dataset1',
            'table_ref' => 'table1',
            'label' => 'Dataset1 label',
            'description' => 'Dataset1 description',
            'display' => 10,
            'count' => 10000,
            'vo' => false,
            'data_path' => '/mnt/dataset1',
            'config' => '{}',
            'public' => true,
            'survey_name' => 'anis_survey',
            'id_dataset_family' => 1
        );
    }
}
