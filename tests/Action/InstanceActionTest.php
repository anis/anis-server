<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use App\Entity\Instance;

final class InstanceActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\InstanceAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testInstanceIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Instance with name aspic is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'aspic'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAnInstanceByName(): void
    {
        $instance = $this->createMock(Instance::class);
        $instance->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($instance);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'aspic'));
    }

    public function testEditAnInstanceEmptyLabelField(): void
    {
        $instance = $this->createMock(Instance::class);
        $this->entityManager->method('find')->willReturn($instance);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to edit the instance');
        $request = $this->getRequest('PUT')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('name' => 'aspic'));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditAnInstance(): void
    {
        $instance = $this->createMock(Instance::class);
        $this->entityManager->method('find')->willReturn($instance);

        $this->entityManager->expects($this->once())->method('flush');

        $fields = array(
            'label' => 'AspiC',
            'client_url' => 'http://aspic.lam.fr',
            'config' => '{}'
        );

        $request = $this->getRequest('PUT')->withParsedBody($fields);
        ($this->action)($request, new Response(), array('name' => 'aspic'));
    }

    public function testDeleteAnInstance(): void
    {
        $instance = $this->createMock(Instance::class);
        $instance->method('getName')->willReturn('aspic');
        $this->entityManager->method('find')->willReturn($instance);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('name' => 'aspic'));
        $this->assertSame(
            json_encode(array('message' => 'Instance with name aspic is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/instance/aspic', array(
            'Content-Type' => 'application/json'
        ));
    }
}
