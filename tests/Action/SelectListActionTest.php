<?php

/*
 * This file is part of Anis Settings.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;

final class SelectListActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\SelectListAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetSelectList(): void
    {
        $repository = $this->createMock(ObjectRepository::class);
        $repository->expects($this->once())->method('findAll');
        $this->entityManager->method('getRepository')->with('App\Entity\Select')->willReturn($repository);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array());
    }

    public function testAddANewSelectEmptyNameField(): void
    {
        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param name needed to add a new select');
        $request = $this->getRequest('POST')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array());
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewSelect(): void
    {
        $this->entityManager->expects($this->once())->method('persist');

        $fields = array(
            'name' => 'search_flag',
            'label' => 'Search flag'
        );

        $request = $this->getRequest('POST')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array());
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/select', array(
            'Content-Type' => 'application/json'
        ));
    }
}
