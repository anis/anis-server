<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Schema\View;
use Doctrine\ORM\EntityManager;
use App\Search\DBALConnectionFactory;
use App\Entity\Database;

final class TableListActionTest extends TestCase
{
    private $action;
    private $entityManager;
    private $connectionFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->connectionFactory = $this->createMock(DBALConnectionFactory::class);
        $this->action = new \App\Action\TableListAction($this->entityManager, $this->connectionFactory);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    public function testDatabaseIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Database with id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetTablesAndViewsList(): void
    {
        $database = $this->createMock(Database::class);
        $this->entityManager->method('find')->willReturn($database);

        $table = $this->createMock(Table::class);
        $table->method('getName')->willReturn('table1');
        $table2 = $this->createMock(Table::class);
        $table2->method('getName')->willReturn('table2');

        $sm = $this->createMock(AbstractSchemaManager::class);
        $sm->method('listTables')->willReturn(array($table, $table2));

        $view = $this->createMock(View::class);
        $view->method('getName')->willReturn('view1');
        $view2 = $this->createMock(View::class);
        $view2->method('getName')->willReturn('view2');

        $sm->method('listViews')->willReturn(array($view, $view2));

        $connection = $this->createMock(Connection::class);
        $connection->method('getSchemaManager')->willReturn($sm);

        $this->connectionFactory->method('create')->willReturn($connection);

        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertSame(json_encode(array('table1', 'table2', 'view1', 'view2')), (string) $response->getBody());
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/database/1/table', array(
            'Content-Type' => 'application/json'
        ));
    }
}
