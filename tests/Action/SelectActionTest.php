<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use App\Entity\Select;

final class SelectActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\SelectAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testSelectIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Select with name search_flag is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'search_flag'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetASelectById(): void
    {
        $select = $this->createMock(Select::class);
        $select->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($select);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'search_flag'));
    }

    public function testEditASelectEmptyLabelField(): void
    {
        $select = $this->createMock(Select::class);
        $this->entityManager->method('find')->willReturn($select);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to edit the select');
        $request = $this->getRequest('PUT')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('name' => 'search_flag'));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditASelect(): void
    {
        $select = $this->createMock(Select::class);
        $this->entityManager->method('find')->willReturn($select);
        $this->entityManager->expects($this->once())->method('flush');

        $fields = array(
            'name' => 'search_flag',
            'label' => 'New label'
        );

        $request = $this->getRequest('PUT')->withParsedBody($fields);
        ($this->action)($request, new Response(), array('name' => 'search_flag'));
    }

    public function testDeleteASelect(): void
    {
        $select = $this->createMock(Select::class);
        $select->method('getName')->willReturn('search_flag');
        $this->entityManager->method('find')->willReturn($select);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('name' => 'search_flag'));
        $this->assertSame(
            json_encode(array('message' => 'Select with name search_flag is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/select/search_flag', array(
            'Content-Type' => 'application/json'
        ));
    }
}
