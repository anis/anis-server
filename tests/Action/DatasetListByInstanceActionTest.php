<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\Expr;
use App\Entity\Instance;

final class DatasetListByInstanceActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $settings = array(
            'enabled' => '0',
            'admin_role' => 'anis_admin'
        );
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\DatasetListByInstanceAction($this->entityManager, $settings);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    public function testInstanceIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Instance with name default is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'default'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAllDatasetsForAnInstance(): void
    {
        $expr = $this->createMock(Expr::class);
        $query = $this->createMock(AbstractQuery::class);
        $query->expects($this->once())->method('getResult');

        $queryBuilder = $this->createMock(QueryBuilder::class);
        $queryBuilder->method('select')->willReturn($queryBuilder);
        $queryBuilder->method('from')->willReturn($queryBuilder);
        $queryBuilder->method('join')->willReturn($queryBuilder);
        $queryBuilder->method('expr')->willReturn($expr);
        $queryBuilder->expects($this->once())->method('getQuery')->willReturn($query);

        $instance = $this->createMock(Instance::class);
        $this->entityManager->method('find')->willReturn($instance);
        $this->entityManager->method('createQueryBuilder')->willReturn($queryBuilder);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'default'));
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/instance/default/dataset', array(
            'Content-Type' => 'application/json'
        ));
    }
}
