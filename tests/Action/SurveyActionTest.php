<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use App\Entity\Survey;
use App\Entity\Database;

final class SurveyActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\SurveyAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testSurveyIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Survey with name anis_survey is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('name' => 'anis_survey'));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetASurveyByName(): void
    {
        $survey = $this->createMock(Survey::class);
        $survey->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($survey);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('name' => 'anis_survey'));
    }

    public function testEditASurveyEmptyLabelField(): void
    {
        $survey = $this->createMock(Survey::class);
        $this->entityManager->method('find')->willReturn($survey);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to edit the survey');
        $request = $this->getRequest('PUT')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('name' => 'anis_survey'));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditASurveyDatabaseNotFound(): void
    {
        $survey = $this->createMock(Survey::class);
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($survey, null);

        $fields = $this->getEditSurveyFields();
        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Database with id 1 is not found');
        $request = $this->getRequest('PUT')->withParsedBody($fields);
        $response = ($this->action)($request, new Response(), array('name' => 'anis_survey'));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditASurvey(): void
    {
        $database = $this->createMock(Database::class);
        $survey = $this->createMock(Survey::class);
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls($survey, $database);

        $this->entityManager->expects($this->once())->method('flush');

        $fields = $this->getEditSurveyFields();

        $request = $this->getRequest('PUT')->withParsedBody($fields, $database);
        ($this->action)($request, new Response(), array('name' => 'anis_survey'));
    }

    public function testDeleteASurvey(): void
    {
        $survey = $this->createMock(Survey::class);
        $survey->method('getName')->willReturn('anis_survey');
        $this->entityManager->method('find')->willReturn($survey);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('name' => 'anis_survey'));
        $this->assertSame(
            json_encode(array('message' => 'Survey anis_survey is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/survey/anis_survey', array(
            'Content-Type' => 'application/json'
        ));
    }

    private function getEditSurveyFields(): array
    {
        return array(
            'label' => 'New label survey',
            'description' => 'Anis survey Test',
            'link' => 'http://survey.com',
            'manager' => 'M. Durand',
            'id_database' => 1
        );
    }
}
