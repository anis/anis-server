<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Tests\Action;

use PHPUnit\Framework\TestCase;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManager;
use App\Entity\Option;

final class OptionActionTest extends TestCase
{
    private $action;
    private $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new \App\Action\OptionAction($this->entityManager);
    }

    public function testOptionsHttpMethod(): void
    {
        $request = $this->getRequest('OPTIONS');
        $response = ($this->action)($request, new Response(), array());
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testOptionIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Option with id 1 is not found');
        $request = $this->getRequest('GET');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAnOptionById(): void
    {
        $option = $this->createMock(Option::class);
        $option->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($option);

        $request = $this->getRequest('GET');
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testEditAnOptionEmptyLabelField(): void
    {
        $option = $this->createMock(Option::class);
        $this->entityManager->method('find')->willReturn($option);

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Param label needed to edit the option');
        $request = $this->getRequest('PUT')->withParsedBody(array());
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testEditAnOption(): void
    {
        $option = $this->createMock(Option::class);
        $this->entityManager->method('find')->willReturn($option);
        $this->entityManager->expects($this->once())->method('flush');

        $fields = array(
            'label' => 'ID',
            'value' => 'id',
            'display' => 20
        );

        $request = $this->getRequest('PUT')->withParsedBody($fields);
        ($this->action)($request, new Response(), array('id' => 1));
    }

    public function testDeleteAnOption(): void
    {
        $option = $this->createMock(Option::class);
        $option->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($option);

        $request = $this->getRequest('DELETE');
        $response = ($this->action)($request, new Response(), array('id' => 1));
        $this->assertSame(
            json_encode(array('message' => 'Option with id 1 is removed!')),
            (string) $response->getBody()
        );
    }

    private function getRequest(string $method): ServerRequest
    {
        return new ServerRequest($method, '/option/1', array(
            'Content-Type' => 'application/json'
        ));
    }
}
