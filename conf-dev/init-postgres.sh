#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER anis LOGIN PASSWORD 'anis';
    CREATE USER keycloak LOGIN PASSWORD 'keycloak';
    CREATE DATABASE anis_metamodel;
    CREATE DATABASE anis_test;
    CREATE DATABASE keycloakdb;
    GRANT ALL PRIVILEGES ON DATABASE anis_metamodel TO anis;
    GRANT ALL PRIVILEGES ON DATABASE anis_test TO anis;
EOSQL
psql -v ON_ERROR_STOP=1 -f /sql/data_test.sql --username "anis" --dbname "anis_test"
