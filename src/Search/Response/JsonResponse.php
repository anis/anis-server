<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Response;

use Psr\Http\Message\ResponseInterface as Response;
use Doctrine\DBAL\Driver\Statement;
use App\Search\Query\AnisQueryBuilder;

/**
 * Class to build the json search response
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Response
 */
class JsonResponse implements IResponse
{
    /**
     * @param  ResponseInterface $response PSR-7   This object represents the HTTP response
     * @param  AnisQueryBuilder  $anisQueryBuilder Object used to wrap the Doctrine DBAL Query Builder
     *
     * @return Response
     */
    public function getResponse(Response $response, AnisQueryBuilder $anisQueryBuilder): Response
    {
        $stmt = $anisQueryBuilder->getDoctrineQueryBuilder()->execute();
        $attributes = $anisQueryBuilder->getAttributesSelected();
        $payload = json_encode($this->decodeNestedJson($stmt, $attributes), JSON_UNESCAPED_SLASHES);
        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Decode each nsted json result and returns array results
     *
     * @param Statement   $stmt       The doctrine statement of the query request
     * @param Attribute[] $attributes The selected attributes for the request
     *
     * @return array
     */
    private function decodeNestedJson(Statement $stmt, array $attributes): array
    {
        $rows = array();
        $jsonAttributes = $this->getAttributesOfTypeJson($attributes);
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            foreach ($row as $key => $column) {
                if (array_search($key, $jsonAttributes) !== false && !is_null($column)) {
                    $row[$key] = json_decode($column, true);
                }
            }
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Returns array of attributes label for the attribute type json
     *
     * @param Attribute[] $attributes The query attributes selected
     *
     * @return string[]
     */
    private function getAttributesOfTypeJson(array $attributes): array
    {
        return array_map(function ($attribute) {
            return $attribute->getLabel();
        }, array_filter($attributes, function ($attribute) {
            return $attribute->getType() === 'json';
        }));
    }
}
