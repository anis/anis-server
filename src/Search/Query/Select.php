<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query;

use App\Entity\Dataset;

/**
 * Represents the Anis Select Query Part
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query
 */
class Select extends AbstractQueryPart
{
    /**
     * Adds the select clause to the request and set attributes selected into the anis query builder
     *
     * @param AnisQueryBuilder $anisQueryBuilder Represents the query being built
     * @param Dataset          $dataset          Represents the requested dataset
     * @param string[]         $queryParams      The query params of the url (after ?)
     */
    public function __invoke(AnisQueryBuilder $anisQueryBuilder, Dataset $dataset, array $queryParams): void
    {
        if ($queryParams['a'] !== 'count' && $queryParams['a'] !== 'all') {
            $listOfIds = explode(';', $queryParams['a']);
            $columns = array();
            $attributes = array();
            foreach ($listOfIds as $id) {
                $attribute = $this->getAttribute($dataset, (int) $id);
                $columns[] = $dataset->getTableRef() . '.' . $attribute->getName() . ' as ' . $attribute->getLabel();
                $attributes[] = $attribute;
            }
            $anisQueryBuilder->getDoctrineQueryBuilder()->select($columns);
            $anisQueryBuilder->setAttributesSelected($attributes);
        }
    }
}
