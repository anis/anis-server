<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query;

use Doctrine\DBAL\Query\Expression\CompositeExpression;
use App\Entity\Dataset;

/**
 * Represents the Anis Cone-Search Query Part
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query
 */
class ConeSearch extends AbstractQueryPart
{
    /**
     * Add cone-search clause to the request using a dataset object
     *
     * @param AnisQueryBuilder $anisQueryBuilder Represents the query being built
     * @param Dataset          $dataset          Represents the requested dataset
     * @param string[]         $queryParams      The query params of the url (after ?)
     */
    public function __invoke(AnisQueryBuilder $anisQueryBuilder, Dataset $dataset, array $queryParams): void
    {
        if (array_key_exists('cs', $queryParams)) {
            $queryBuilder = $anisQueryBuilder->getDoctrineQueryBuilder();
            $params = explode(':', $queryParams['cs']);
            if (count($params) !== 3) {
                throw SearchQueryException::badNumberOfParamsForConeSearch();
            }

            list($ra, $dec, $radius) = $params;
            $ra = floatval($ra);
            $dec = floatval($dec);
            $radius = floatval($radius);
            $coneSearchConfig = $dataset->getConfig()['cone_search'];

            if ($coneSearchConfig['enabled'] !== true) {
                throw SearchQueryException::coneSearchUnavailable();
            }

            $attributeRa = $this->getAttribute($dataset, $coneSearchConfig['column_ra']);
            $attributeDec = $this->getAttribute($dataset, $coneSearchConfig['column_dec']);
            $columnRa = $dataset->getTableRef() . '.' . $attributeRa->getName();
            $columnDec = $dataset->getTableRef() . '.' . $attributeDec->getName();

            $cdcl2 = pow(cos($dec * (M_PI * 2) / 360), 2);
            if ($radius == 0) {
                $radius = 1 / 1000000;
            }
            $radius2 = pow($radius, 2);

            $raddeg = $radius / 3600;
            $decmin = $dec - $raddeg;
            $decmax = $dec + $raddeg;
            if ($decmin < -90 || $decmax > 90) {
                $ramin = 0;
                $ramax = 360;
            } else {
                $ra_corrected_radius = $raddeg / cos(deg2rad(abs($dec) + $raddeg));
                $ramin = $ra - $ra_corrected_radius;
                $ramax = $ra + $ra_corrected_radius;
            }

            $coneSearchValue = '(' . $cdcl2;
            $coneSearchValue .= ' * (' . $ra . ' - ' . $columnRa . ') * (' . $ra . ' - ' . $columnRa . '))';
            $coneSearchValue .= ' + ((' . $dec . ' - ' . $columnDec . ') * (' . $dec . ' - ' . $columnDec . '))';
            $coneSearchCriterion = $queryBuilder->expr()->lte($coneSearchValue, $radius2);
            $raCriterion = (new CompositeExpression(CompositeExpression::TYPE_AND, [
                $queryBuilder->expr()->gte($columnRa, $ramin),
                $queryBuilder->expr()->lte($columnRa, $ramax)
            ]));
            $decCriterion = (new CompositeExpression(CompositeExpression::TYPE_AND, [
                $queryBuilder->expr()->gte($columnDec, $decmin),
                $queryBuilder->expr()->lte($columnDec, $decmax)
            ]));
            $queryBuilder->where(new CompositeExpression(CompositeExpression::TYPE_AND, [
                $coneSearchCriterion,
                $raCriterion,
                $decCriterion
            ]));
        }
    }
}
