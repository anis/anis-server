<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query;

use App\Entity\Dataset;

/**
 * Represents the Anis Order Query Part
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query
 */
class Order extends AbstractQueryPart
{
    /**
     * Add order clause to the request
     *
     * @param AnisQueryBuilder $anisQueryBuilder Represents the query being built
     * @param Dataset          $dataset          Represents the requested dataset
     * @param string[]         $queryParams      The query params of the url (after ?)
     */
    public function __invoke(AnisQueryBuilder $anisQueryBuilder, Dataset $dataset, array $queryParams): void
    {
        if (array_key_exists('o', $queryParams)) {
            $orders = explode(';', $queryParams['o']);
            foreach ($orders as $order) {
                $o = explode(':', $order);
                if (count($o) != 2) {
                    throw SearchQueryException::badNumberOfParamsForOrder();
                }
                if ($o[1] === 'a') {
                    $aord = 'ASC';
                } elseif ($o[1] === 'd') {
                    $aord = 'DESC';
                } else {
                    throw SearchQueryException::typeOfOrderDoesNotExist($o[1]);
                }
                $attribute = $this->getAttribute($dataset, (int) $o[0]);
                $anisQueryBuilder->getDoctrineQueryBuilder()->orderBy(
                    $dataset->getTableRef() . '.' . $attribute->getName(),
                    $aord
                );
            }
        }
    }
}
