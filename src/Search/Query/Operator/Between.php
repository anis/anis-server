<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\DBAL\Query\Expression\CompositeExpression;

/**
 * Operator that represents a between of a where clause
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query\Operator
 */
class Between extends Operator
{
    /**
     * First value for the between
     *
     * @var string
     */
    private $value1;

    /**
     * Second value for the between
     *
     * @var string
     */
    private $value2;

    /**
     * Create the class before call getExpression method to execute this operator
     *
     * @param ExpressionBuilder $expr
     * @param string            $column
     * @param string            $columnType
     * @param string            $value1
     * @param string            $value2
     */
    public function __construct(
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        string $value1,
        string $value2
    ) {
        parent::__construct($expr, $column, $columnType);
        $this->verifyTypeCompatibility($value1);
        $this->verifyTypeCompatibility($value2);
        $this->value1 = $value1;
        $this->value2 = $value2;
    }

    /**
     * This method create a new object "composite expression" (Doctrine class)
     * that represents the between criterion
     *
     * @return string
     */
    public function getExpression(): string
    {
        $args = array();
        $args[] = $this->expr->gte($this->column, $this->getSqlValue($this->value1));
        $args[] = $this->expr->lte($this->column, $this->getSqlValue($this->value2));
        return (string) (new CompositeExpression(CompositeExpression::TYPE_AND, $args));
    }
}
