<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query;

use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Search\Query\IQueryPart;
use App\Entity\Dataset;
use App\Entity\Attribute;

/**
 * Class used to wrap the Doctrine DBAL Query Builder
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Search\Query
 */
class AnisQueryBuilder
{
    /**
     * @var DoctrineQueryBuilder
     */
    private $doctrineQueryBuilder;

    /**
     * @var IQueryPart[]
     */
    private $queryParts;

    /**
     * @var Dataset
     */
    private $datasetSelected;

    /**
     * @var Attribute[]
     */
    private $attributesSelected;

    /**
     * Returns Doctrine DBAL query builder
     *
     * @return DoctrineQueryBuilder
     */
    public function getDoctrineQueryBuilder(): DoctrineQueryBuilder
    {
        return $this->doctrineQueryBuilder;
    }

    /**
     * Set Doctrine DBAL Query Builder
     *
     * @param DoctrineQueryBuilder $doctrineQueryBuilder Represents the doctrine DBAL query being built
     */
    public function setDoctrineQueryBuilder(DoctrineQueryBuilder $doctrineQueryBuilder): void
    {
        $this->doctrineQueryBuilder = $doctrineQueryBuilder;
    }

    /**
     * Returns the query dataset selected
     */
    public function getDatasetSelected(): Dataset
    {
        return $this->datasetSelected;
    }

    /**
     * Keeps the query dataset selected
     */
    public function setDatasetSelected(Dataset $datasetSelected): void
    {
        $this->datasetSelected = $datasetSelected;
    }

    /**
     * Returns the query attributes selected
     *
     * @return Attribute[]
     */
    public function getAttributesSelected(): array
    {
        return $this->attributesSelected;
    }

    /**
     * Keeps the query attributes selected
     *
     * @param Attribute[] $attributesSelected Attributes selected
     */
    public function setAttributesSelected(array $attributesSelected): void
    {
        $this->attributesSelected = $attributesSelected;
    }

    /**
     * Adding a query part function will be executed to build the final query
     *
     * @param IQueryPart $queryPart The query part object like, for example, Where
     * @return AnisQueryBuilder
     */
    public function addQueryPart(IQueryPart $queryPart): AnisQueryBuilder
    {
        $this->queryParts[] = $queryPart;
        return $this;
    }

    /**
     * Executes all query parts added to build the final query
     *
     * @param string[] $queryParams The query params of the url (after ?)
     */
    public function build(array $queryParams): void
    {
        foreach ($this->queryParts as $queryPart) {
            $queryPart($this, $this->datasetSelected, $queryParams);
        }
    }
}
