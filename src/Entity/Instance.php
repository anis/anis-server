<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="instance")
 */
class Instance implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="string", name="client_url", nullable=true)
     */
    protected $clientUrl;

    /**
     * @var string
     *
     * @Column(type="json", nullable=true)
     */
    protected $config;

    /**
     * @var DatasetFamily[]
     *
     * @OneToMany(targetEntity="DatasetFamily", mappedBy="instance")
     */
    protected $datasetFamilies;

    public function __construct(string $name, string $label)
    {
        $this->name = $name;
        $this->label = $label;
        $this->datasetFamilies = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    public function getClientUrl()
    {
        return $this->clientUrl;
    }

    public function setClientUrl($clientUrl)
    {
        $this->clientUrl = $clientUrl;
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function getDatasetFamilies()
    {
        return $this->datasetFamilies;
    }

    public function getNbDatasets()
    {
        $nbDatasets = 0;
        foreach ($this->datasetFamilies as $family) {
            $nbDatasets += count($family->getDatasets());
        }
        return $nbDatasets;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'client_url' => $this->getClientUrl(),
            'config' => $this->getConfig(),
            'nb_dataset_families' => count($this->getDatasetFamilies()),
            'nb_datasets' => $this->getNbDatasets()
        ];
    }
}
