<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="settings_option")
 */
class Option implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $value;

    /**
     * @var integer
     *
     * @Column(type="integer", nullable=false)
     */
    protected $display;

    /**
     * @var Select
     *
     * @ManyToOne(targetEntity="Select", inversedBy="options")
     * @JoinColumn(name="select_name", referencedColumnName="name", nullable=false, onDelete="CASCADE")
     */
    protected $select;

    public function __construct(Select $select)
    {
        $this->select = $select;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    public function getDisplay(): int
    {
        return $this->display;
    }

    public function setDisplay(int $display): void
    {
        $this->display = (int) $display;
    }

    public function getSelect(): Select
    {
        return $this->select;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'value' => $this->getValue(),
            'display' => $this->getDisplay(),
            'select_name' => $this->getSelect()->getName()
        ];
    }
}
