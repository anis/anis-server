<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="settings_select")
 */
class Select implements \JsonSerializable
{
    /**
     * @var string
     *
     * @Id
     * @Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var Option[]
     *
     * @OneToMany(targetEntity="Option", mappedBy="select")
     */
    protected $options;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'label' => $this->getLabel()
        ];
    }
}
