<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="attribute")
 */
class Attribute implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @var Dataset
     *
     * @Id
     * @ManyToOne(targetEntity="Dataset", inversedBy="attributes")
     * @JoinColumn(name="dataset_name", referencedColumnName="name", nullable=false, onDelete="CASCADE")
     */
    protected $dataset;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="string", name="form_label", nullable=false)
     */
    protected $formLabel;

    /**
     * @var string
     *
     * @Column(type="string", nullable=true)
     */
    protected $description;

    /**
     * @var int
     *
     * @Column(type="integer", name="output_display", nullable=true)
     */
    protected $outputDisplay;

    /**
     * @var int
     *
     * @Column(type="integer", name="criteria_display", nullable=true)
     */
    protected $criteriaDisplay;

    /**
     * @var string
     *
     * @Column(type="string", name="search_flag", nullable=true)
     */
    protected $searchFlag;

    /**
     * @var string
     *
     * @Column(type="string", name="search_type", nullable=true)
     */
    protected $searchType;

    /**
     * @var string
     *
     * @Column(type="string", nullable=true)
     */
    protected $operator;

    /**
     * @var string
     *
     * @Column(type="string", name="type", nullable=true)
     */
    protected $type;

    /**
     * @var string
     *
     * @Column(type="string", nullable=true)
     */
    protected $min;

    /**
     * @var string
     *
     * @Column(type="string", nullable=true)
     */
    protected $max;

    /**
     * @var string
     *
     * @Column(type="string", name="placeholder_min", nullable=true)
     */
    protected $placeholderMin;

    /**
     * @var string
     *
     * @Column(type="string", name="placeholder_max", nullable=true)
     */
    protected $placeholderMax;

    /**
     * @var string
     *
     * @Column(type="string", name="renderer", nullable=true)
     */
    protected $renderer;

    /**
     * @var string
     *
     * @Column(type="json", name="renderer_config", nullable=true)
     */
    protected $rendererConfig;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="selected", nullable=false, options={"default":false})
     */
    protected $selected = false;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="order_by", nullable=false, options={"default":false})
     */
    protected $orderBy = false;

    /**
     * @var int
     *
     * @Column(type="integer", name="order_display", nullable=true)
     */
    protected $orderDisplay;

    /**
     * @var bool
     *
     * @Column(type="boolean", name="detail", nullable=false, options={"default":false})
     */
    protected $detail = false;

    /**
     * @var int
     *
     * @Column(type="integer", name="display_detail", nullable=true)
     */
    protected $displayDetail;

    /**
     * @var string
     *
     * @Column(type="string", name="renderer_detail", nullable=true)
     */
    protected $rendererDetail;

    /**
     * @var string
     *
     * @Column(type="json", name="renderer_detail_config", nullable=true)
     */
    protected $rendererDetailConfig;

    /**
     * @var string
     *
     * @Column(type="json", name="options", nullable=true)
     */
    protected $options;

    /**
     * @var string
     *
     * @Column(type="string", name="vo_utype", nullable=true)
     */
    protected $voUtype;

    /**
     * @var string
     *
     * @Column(type="string", name="vo_ucd", nullable=true)
     */
    protected $voUcd;

    /**
     * @var string
     *
     * @Column(type="string", name="vo_unit", nullable=true)
     */
    protected $voUnit;

    /**
     * @var string
     *
     * @Column(type="string", name="vo_description", nullable=true)
     */
    protected $voDescription;

    /**
     * @var string
     *
     * @Column(type="string", name="vo_datatype", nullable=true)
     */
    protected $voDatatype;

    /**
     * @var int
     *
     * @Column(type="integer", name="vo_size", nullable=true)
     */
    protected $voSize;

    /**
     * @var CriteriaFamily
     *
     * @ManyToOne(targetEntity="CriteriaFamily")
     * @JoinColumn(name="criteria_family", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $criteriaFamily;

    /**
     * @var OutputCategory
     *
     * @ManyToOne(targetEntity="OutputCategory")
     * @JoinColumn(name="output_category", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $outputCategory;

    public function __construct(int $id, Dataset $dataset)
    {
        $this->id = $id;
        $this->dataset = $dataset;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDataset()
    {
        return $this->dataset;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getFormLabel()
    {
        return $this->formLabel;
    }

    public function setFormLabel($formLabel)
    {
        $this->formLabel = $formLabel;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getOutputDisplay()
    {
        return $this->outputDisplay;
    }

    public function setOutputDisplay($outputDisplay)
    {
        $this->outputDisplay = $outputDisplay;
    }

    public function setCriteriaDisplay($criteriaDisplay)
    {
        $this->criteriaDisplay = $criteriaDisplay;
    }

    public function getCriteriaDisplay()
    {
        return $this->criteriaDisplay;
    }

    public function getSearchFlag()
    {
        return $this->searchFlag;
    }

    public function setSearchFlag($searchFlag)
    {
        $this->searchFlag = $searchFlag;
    }

    public function getSearchType()
    {
        return $this->searchType;
    }

    public function setSearchType($searchType)
    {
        $this->searchType = $searchType;
    }

    public function getOperator()
    {
        return $this->operator;
    }

    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getMin()
    {
        return $this->min;
    }

    public function setMin($min)
    {
        $this->min = $min;
    }

    public function getMax()
    {
        return $this->max;
    }

    public function setMax($max)
    {
        $this->max = $max;
    }

    public function getPlaceholderMin()
    {
        return $this->placeholderMin;
    }

    public function setPlaceholderMin($placeholderMin)
    {
        $this->placeholderMin = $placeholderMin;
    }

    public function getPlaceholderMax()
    {
        return $this->placeholderMax;
    }

    public function setPlaceholderMax($placeholderMax)
    {
        $this->placeholderMax = $placeholderMax;
    }

    public function getRendererConfig()
    {
        return $this->rendererConfig;
    }

    public function setRendererConfig($rendererConfig)
    {
        $this->rendererConfig = $rendererConfig;
    }

    public function getRenderer()
    {
        return $this->renderer;
    }

    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;
    }

    public function getDisplayDetail()
    {
        return $this->displayDetail;
    }

    public function setDisplayDetail($displayDetail)
    {
        $this->displayDetail = $displayDetail;
    }

    public function getVoUtype()
    {
        return $this->voUtype;
    }

    public function setVoUtype($voUtype)
    {
        $this->voUtype = $voUtype;
    }

    public function getVoUcd()
    {
        return $this->voUcd;
    }

    public function setVoUcd($voUcd)
    {
        $this->voUcd = $voUcd;
    }

    public function getVoUnit()
    {
        return $this->voUnit;
    }

    public function setVoUnit($voUnit)
    {
        $this->voUnit = $voUnit;
    }

    public function getVoDescription()
    {
        return $this->voDescription;
    }

    public function setVoDescription($voDescription)
    {
        $this->voDescription = $voDescription;
    }

    public function getVoDatatype()
    {
        return $this->voDatatype;
    }

    public function setVoDatatype($voDatatype)
    {
        $this->voDatatype = $voDatatype;
    }

    public function getVoSize()
    {
        return $this->voSize;
    }

    public function setVoSize($voSize)
    {
        $this->voSize = $voSize;
    }

    public function getSelected()
    {
        return $this->selected;
    }

    public function setSelected($selected)
    {
        $this->selected = $selected;
    }

    public function getOrderBy()
    {
        return $this->orderBy;
    }

    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
    }

    public function getOrderDisplay()
    {
        return $this->orderDisplay;
    }

    public function setOrderDisplay($orderDisplay)
    {
        $this->orderDisplay = $orderDisplay;
    }

    public function getDetail()
    {
        return $this->detail;
    }

    public function setDetail($detail)
    {
        $this->detail = $detail;
    }

    public function getRendererDetail()
    {
        return $this->rendererDetail;
    }

    public function setRendererDetail($rendererDetail)
    {
        $this->rendererDetail = $rendererDetail;
    }

    public function getRendererDetailConfig()
    {
        return $this->rendererDetailConfig;
    }

    public function setRendererDetailConfig($rendererDetailConfig)
    {
        $this->rendererDetailConfig = $rendererDetailConfig;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions($options)
    {
        $this->options = $options;
    }

    public function getCriteriaFamily()
    {
        return $this->criteriaFamily;
    }

    public function setCriteriaFamily($criteriaFamily)
    {
        $this->criteriaFamily = $criteriaFamily;
    }

    public function getOutputCategory()
    {
        return $this->outputCategory;
    }

    public function setOutputCategory($outputCategory)
    {
        $this->outputCategory = $outputCategory;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'form_label' => $this->getFormLabel(),
            'description' => $this->getDescription(),
            'output_display' => $this->getOutputDisplay(),
            'criteria_display' => $this->getCriteriaDisplay(),
            'search_flag' => $this->getSearchFlag(),
            'search_type' => $this->getSearchType(),
            'type' => $this->getType(),
            'operator' => $this->getOperator(),
            'min' => $this->getMin(),
            'max' => $this->getMax(),
            'placeholder_min' => $this->getPlaceholderMin(),
            'placeholder_max' => $this->getPlaceholderMax(),
            'renderer' => $this->getRenderer(),
            'renderer_config' => $this->getRendererConfig(),
            'selected' => $this->getSelected(),
            'order_by' => $this->getOrderBy(),
            'order_display' => $this->getOrderDisplay(),
            'detail' => $this->getDetail(),
            'display_detail' => $this->getDisplayDetail(),
            'renderer_detail' => $this->getRendererDetail(),
            'renderer_detail_config' => $this->getRendererDetailConfig(),
            'options' => $this->getOptions(),
            'vo_utype' => $this->getVoUtype(),
            'vo_ucd' => $this->getVoUcd(),
            'vo_unit' => $this->getVoUnit(),
            'vo_description' => $this->getVoDescription(),
            'vo_datatype' => $this->getVoDatatype(),
            'vo_size' => $this->getVoSize(),
            'id_criteria_family' => is_null($this->getCriteriaFamily()) ? null : $this->getCriteriaFamily()->getId(),
            'id_output_category' => is_null($this->getOutputCategory()) ? null : $this->getOutputCategory()->getId()
        ];
    }
}
