<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="database")
 */
class Database implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $dbname;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $type;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $host;

    /**
     * @var int
     *
     * @Column(type="integer", nullable=true)
     */
    protected $port;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $login;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $password;

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getDbName()
    {
        return $this->dbname;
    }

    public function setDbName($dbname)
    {
        $this->dbname = $dbname;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function setPort($port)
    {
        $this->port = $port;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'dbname' => $this->getDbName(),
            'dbtype' => $this->getType(),
            'dbhost' => $this->getHost(),
            'dbport' => $this->getPort(),
            'dblogin' => $this->getLogin(),
            'dbpassword' => $this->getPassword()
        ];
    }
}
