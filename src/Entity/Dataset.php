<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="dataset")
 */
class Dataset implements \JsonSerializable
{
    /**
     * @var string
     *
     * @Id
     * @Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", name="table_ref", nullable=false)
     */
    protected $tableRef;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var int
     *
     * @Column(type="integer", nullable=false)
     */
    protected $display;

    /**
     * @var string
     *
     * @Column(type="string", name="data_path", nullable=true)
     */
    protected $dataPath;

    /**
     * @var array
     *
     * @Column(type="json", nullable=true)
     */
    protected $config;

    /**
     * @var bool
     *
     * @Column(type="boolean", nullable=false)
     */
    protected $public;

    /**
     * @var Survey
     *
     * @ManyToOne(targetEntity="Survey", inversedBy="datasets")
     * @JoinColumn(name="survey_name", referencedColumnName="name", nullable=false)
     */
    protected $survey;

    /**
     * @var DatasetFamily
     *
     * @ManyToOne(targetEntity="DatasetFamily", inversedBy="datasets")
     * @JoinColumn(name="id_dataset_family", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $datasetFamily;

    /**
     * @var Attribute[]
     *
     * @OneToMany(targetEntity="Attribute", mappedBy="dataset")
     */
    protected $attributes;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->attributes = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getTableRef()
    {
        return $this->tableRef;
    }

    public function setTableRef($tableRef)
    {
        $this->tableRef = $tableRef;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function setDisplay($display)
    {
        $this->display = $display;
    }

    public function getDataPath()
    {
        return $this->dataPath;
    }

    public function setDataPath($dataPath)
    {
        $this->dataPath = $dataPath;
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function getPublic()
    {
        return $this->public;
    }

    public function setPublic($public)
    {
        $this->public = $public;
    }

    public function getSurvey()
    {
        return $this->survey;
    }

    public function setSurvey($survey)
    {
        $this->survey = $survey;
    }

    public function getDatasetFamily()
    {
        return $this->datasetFamily;
    }

    public function setDatasetFamily($datasetFamily)
    {
        $this->datasetFamily = $datasetFamily;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'table_ref' => $this->getTableRef(),
            'label' => $this->getLabel(),
            'description' => $this->getDescription(),
            'display' => $this->getDisplay(),
            'data_path' => $this->getDataPath(),
            'config' => $this->getConfig(),
            'public' => $this->getPublic(),
            'survey_name' => $this->getSurvey()->getName(),
            'id_dataset_family' => $this->getDatasetFamily()->getId()
        ];
    }
}
