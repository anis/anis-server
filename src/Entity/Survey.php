<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Entity
 *
 * @Entity
 * @Table(name="survey")
 */
class Survey implements \JsonSerializable
{
    /**
     * @var string
     *
     * @Id
     * @Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="text", nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @Column(type="string", nullable=true)
     */
    protected $link;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    protected $manager;

    /**
     * @var Database
     *
     * @ManyToOne(targetEntity="Database")
     * @JoinColumn(name="database_id", referencedColumnName="id", nullable=false)
     */
    protected $database;

    /**
     * @var Dataset[]
     *
     * @OneToMany(targetEntity="Dataset", mappedBy="survey")
     */
    protected $datasets;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->datasets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink($link)
    {
        $this->link = $link;
    }

    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function getDatasets()
    {
        return $this->datasets;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'description' => $this->getDescription(),
            'link' => $this->getLink(),
            'manager' => $this->getManager(),
            'id_database' => $this->getDatabase()->getId(),
            'nb_datasets' => count($this->getDatasets())
        ];
    }
}
