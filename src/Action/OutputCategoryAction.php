<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\OutputCategory;
use App\Entity\OutputFamily;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class OutputCategoryAction extends AbstractAction
{
    /**
     * `GET` Returns the output category found
     * `PUT` Full update the output category and returns the new version
     * `DELETE` Delete the output category found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct output category with primary key (id)
        $outputCategory = $this->em->find('App\Entity\OutputCategory', $args['id']);

        // If output category is not found 404
        if (is_null($outputCategory)) {
            throw new HttpNotFoundException(
                $request,
                'Output category with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($outputCategory);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            foreach (array('label', 'display', 'id_output_family') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the output category'
                    );
                }
            }

            // Search the correct output family with primary key
            $idOutputFamily = $parsedBody['id_output_family'];
            $outputFamily = $this->em->find('App\Entity\OutputFamily', $idOutputFamily);

            // Output family is mandatory. If is null 400
            if (is_null($outputFamily)) {
                throw new HttpBadRequestException(
                    $request,
                    'Output family with id ' . $idOutputFamily . ' is not found'
                );
            }

            $this->editOutputCategory($outputCategory, $parsedBody, $outputFamily);
            $payload = json_encode($outputCategory);
        }

        if ($request->getMethod() === DELETE) {
            $id = $outputCategory->getId();
            $this->em->remove($outputCategory);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Output category with id ' . $id . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update output category object with setters
     *
     * @param OutputCategory $outputCategory The output category to update
     * @param array          $parsedBody     Contains the new values ​​of the output category sent by the user
     * @param OutputFamily   $outputFamily   Contains the output family doctrine object to set to the output category
     */
    private function editOutputCategory(
        OutputCategory $outputCategory,
        array $parsedBody,
        OutputFamily $outputFamily
    ): void {
        $outputCategory->setLabel($parsedBody['label']);
        $outputCategory->setDisplay($parsedBody['display']);
        $outputCategory->setOutputFamily($outputFamily);
        $this->em->flush();
    }
}
