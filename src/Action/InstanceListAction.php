<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use App\Entity\Instance;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class InstanceListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all instances listed in the metamodel
     * `POST` Add a new instance
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        if ($request->getMethod() === GET) {
            $instances = $this->em->getRepository('App\Entity\Instance')->findAll();
            $payload = json_encode($instances);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // To work this action needs user information to update
            foreach (array('name', 'label') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new instance'
                    );
                }
            }

            $instance = $this->postInstance($parsedBody);
            $payload = json_encode($instance);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Add a new instance into the metamodel
     *
     * @param array $parsedBody Contains the values ​​of the new instance sent by the user
     *
     * @return Instance
     */
    private function postInstance(array $parsedBody): Instance
    {
        $instance = new Instance($parsedBody['name'], $parsedBody['label']);
        if (!$this->isEmptyField('client_url', $parsedBody)) {
            $instance->setClientUrl($parsedBody['client_url']);
        }
        $instance->setConfig($parsedBody['config']);

        $this->em->persist($instance);
        $this->em->flush();

        return $instance;
    }
}
