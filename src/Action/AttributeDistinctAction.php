<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use App\Search\DBALConnectionFactory;
use PDO;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class AttributeDistinctAction extends AbstractAction
{
    /**
     * @var DBALConnectionFactory
     */
    private $connectionFactory;

    /**
     * Create the classe before call __invoke to execute the action
     *
     * @param EntityManagerInterface $em Doctrine       Entity Manager Interface
     * @param DBALConnectionFactory  $connectionFactory Factory used to construct connection to business database
     */
    public function __construct(EntityManagerInterface $em, DBALConnectionFactory $connectionFactory)
    {
        parent::__construct($em);
        $this->connectionFactory = $connectionFactory;
    }

    /**
     * `GET` Returns a list of distinct values found for this attribute into the business database
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        $attribute = $this->em->getRepository('App\Entity\Attribute')->findOneBy(
            array('dataset' => $args['name'], 'id' => $args['id'])
        );

        // If attribute is not found 404
        if (is_null($attribute)) {
            throw new HttpNotFoundException(
                $request,
                'Attribute with dataset name ' . $args['name'] . ' and attribute id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            // Create query builder with from clause using dataset information
            // With select, group by and order by clauses using attribute information
            $dataset = $attribute->getDataset();
            $column = $dataset->getTableRef() . '.' . $attribute->getName();
            $connection = $this->connectionFactory->create($dataset->getSurvey()->getDatabase());
            $queryBuilder = $connection->createQueryBuilder();
            $queryBuilder->select($column . ' as ' . $attribute->getLabel());
            $queryBuilder->from($dataset->getTableRef());
            $queryBuilder->groupBy($column);
            $queryBuilder->orderBy($column);

            // Execute query and returns response
            $stmt = $queryBuilder->execute();
            $rows = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
            $payload = json_encode($rows);
        }

        $response->getBody()->write($payload);
        return $response;
    }
}
