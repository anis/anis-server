<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use App\Entity\Database;
use App\Entity\Survey;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class SurveyListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all surveys listed in the metamodel database
     * `POST` Add a new survey
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        if ($request->getMethod() === GET) {
            $surveys = $this->em->getRepository('App\Entity\Survey')->findAll();
            $payload = json_encode($surveys, JSON_UNESCAPED_SLASHES);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // To work this action needs information to add a survey
            foreach (array('name', 'label', 'description', 'link', 'manager', 'id_database') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new survey'
                    );
                }
            }

            // Database exists ?
            $idDatabase = $parsedBody['id_database'];
            $database = $this->em->find('App\Entity\Database', $idDatabase);
            if (is_null($database)) {
                throw new HttpBadRequestException(
                    $request,
                    'Database with id ' . $idDatabase . ' is not found'
                );
            }

            $survey = $this->postSurvey($parsedBody, $database);
            $payload = json_encode($survey, JSON_UNESCAPED_SLASHES);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * @param array    $parsedBody Contains the values ​​of the new survey sent by the user
     * @param Database $select     Database on which the survey refers
     *
     * @return Survey
     */
    private function postSurvey(array $parsedBody, Database $database): Survey
    {
        $survey = new Survey($parsedBody['name']);
        $survey->setLabel($parsedBody['label']);
        $survey->setDescription($parsedBody['description']);
        $survey->setLink($parsedBody['link']);
        $survey->setManager($parsedBody['manager']);
        $survey->setDatabase($database);

        $this->em->persist($survey);
        $this->em->flush();

        return $survey;
    }
}
