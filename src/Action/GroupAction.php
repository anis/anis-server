<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Group;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class GroupAction extends AbstractAction
{
    /**
     * `GET` Returns the Group found
     * `PUT` Full update the Group and returns the new version
     * `DELETE` Delete the Group found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct group with primary key
        $group = $this->em->find('App\Entity\Group', $args['id']);

        // If group is not found 404
        if (is_null($group)) {
            throw new HttpNotFoundException(
                $request,
                'Group with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($group);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            foreach (array('role', 'datasets') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the group'
                    );
                }
            }

            $this->editGroup($group, $parsedBody);
            $payload = json_encode($group);
        }

        if ($request->getMethod() === DELETE) {
            $id = $group->getId();
            $this->em->remove($group);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Group with id ' . $id . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update group object with setters
     *
     * @param Group $group The group to update
     * @param array $parsedBody Contains the new values ​​of the group sent by the user
     */
    private function editGroup(Group $group, array $parsedBody): void
    {
        $group->setRole($parsedBody['role']);
        $group->setDatasets($this->getDatasets($parsedBody['datasets']));
        $this->em->flush();
    }

    /**
     * Retrieves list of datasets by list of datasets names
     *
     * @param string[] $listOfDatasetsNames List of datasets names
     *
     * @return Dataset[] List of datasets found
     */
    private function getDatasets(array $listOfDatasetsNames): array
    {
        if (count($listOfDatasetsNames) < 1) {
            return array();
        }

        $in = implode(',', array_map(function ($d) {
            return "'" . $d . "'";
        }, $listOfDatasetsNames));

        $dql = 'SELECT d FROM App\Entity\Dataset d WHERE d.name IN (' . $in . ')';
        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }
}
