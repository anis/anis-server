<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Entity\Select;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class SelectAction extends AbstractAction
{
    /**
     * `GET`    Returns one select by ID
     * `PUT`    Edit one select
     * `DELETE` Delete one select
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct select with primary key (name)
        $select = $this->em->find('App\Entity\Select', $args['name']);

        // If select is not found 404
        if (is_null($select)) {
            throw new HttpNotFoundException(
                $request,
                'Select with name ' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($select);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            foreach (array('label') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the select'
                    );
                }
            }

            $this->editSelect($select, $parsedBody);
            $payload = json_encode($select);
        }

        if ($request->getMethod() === DELETE) {
            $name = $select->getName();
            $this->em->remove($select);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Select with name ' . $name . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
    * Update select object with setters
    *
    * @param Select $select     The select object to update
    * @param array  $parsedBody Contains the new values ​​of the select sent by the user
    */
    private function editSelect(Select $select, array $parsedBody): void
    {
        $select->setLabel($parsedBody['label']);
        $this->em->flush();
    }
}
