<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Instance;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class InstanceAction extends AbstractAction
{
    /**
     * `GET` Returns the instance found
     * `PUT` Full update the instance and returns the new version
     * `DELETE` Delete the instance found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct instance with primary key
        $instance = $this->em->find('App\Entity\Instance', $args['name']);

        // If instance is not found 404
        if (is_null($instance)) {
            throw new HttpNotFoundException(
                $request,
                'Instance with name ' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($instance);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            foreach (array('label') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the instance'
                    );
                }
            }

            $this->editInstance($instance, $parsedBody);
            $payload = json_encode($instance);
        }

        if ($request->getMethod() === DELETE) {
            $name = $instance->getName();
            $this->em->remove($instance);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Instance with name ' . $name . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update instance object with setters
     *
     * @param Instance $instance   The instance to update
     * @param array    $parsedBody Contains the new values ​​of the instance sent by the user
     */
    private function editInstance(Instance $instance, array $parsedBody): void
    {
        $instance->setLabel($parsedBody['label']);
        if (!$this->isEmptyField('client_url', $parsedBody)) {
            $instance->setClientUrl($parsedBody['client_url']);
        } else {
            $instance->setClientUrl(null);
        }
        $instance->setConfig($parsedBody['config']);
        $this->em->flush();
    }
}
