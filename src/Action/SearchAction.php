<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use App\Search\DBALConnectionFactory;
use App\Search\Query\AnisQueryBuilder;
use App\Search\Response\IResponseFactory;
use App\Search\SearchException;

/**
 * Search action
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class SearchAction extends AbstractAction
{
    /**
     * @var DBALConnectionFactory
     */
    private $connectionFactory;

    /**
     * @var AnisQueryBuilder
     */
    private $anisQueryBuilder;

    /**
     * @var IResponseFactory
     */
    private $responseFactory;

    /**
     * Contains settings to handle Json Web Token (app/settings.php)
     *
     * @var array
     */
    private $settings;

    /**
     * Create the classe before call __invoke to execute the action
     *
     * @param EntityManagerInterface $em Doctrine       Entity Manager Interface
     * @param DBALConnectionFactory  $connectionFactory Factory used to construct connection to business database
     * @param AnisQueryBuilder       $anisQueryBuilder  Object used to wrap the Doctrine DBAL Query Builder
     * @param IResponseFactory       $responseFactory   Contains the factory used to return formatted response
     * @param array                  $settings          Settings about token
     */
    public function __construct(
        EntityManagerInterface $em,
        DBALConnectionFactory $connectionFactory,
        AnisQueryBuilder $anisQueryBuilder,
        IResponseFactory $responseFactory,
        array $settings
    ) {
        parent::__construct($em);
        $this->connectionFactory = $connectionFactory;
        $this->anisQueryBuilder = $anisQueryBuilder;
        $this->responseFactory = $responseFactory;
        $this->settings = $settings;
    }

    /**
     * Builds the query according to the parameters of the URL
     * Returns the formatted response (json, csv, votable)
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct dataset with primary key
        $dataset = $this->em->find('App\Entity\Dataset', $args['dname']);

        // If dataset is not found 404
        if (is_null($dataset)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset with name ' . $args['dname'] . ' is not found'
            );
        }

        // If dataset is private and authorization enabled
        if (!$dataset->getPublic() && boolval($this->settings['enabled'])) {
            $this->verifyDatasetAuthorization($request, $dataset->getName(), $this->settings['admin_role']);
        }

        $queryParams = $request->getQueryParams();

        // The parameter "a" is mandatory
        if (!array_key_exists('a', $queryParams)) {
            throw new HttpBadRequestException(
                $request,
                'Param a is required for this request'
            );
        }

        try {
            // Configure the Anis Query Builder
            $connection = $this->connectionFactory->create($dataset->getSurvey()->getDatabase());
            $this->anisQueryBuilder->setDoctrineQueryBuilder($connection->createQueryBuilder());
            $this->anisQueryBuilder->setDatasetSelected($dataset);

            // Build the SQL request
            $this->anisQueryBuilder->build($queryParams);

            // The parameter f is not mandatory and represents the output format
            // By default the format is JSON
            $format = (array_key_exists('f', $queryParams)) ? $queryParams['f'] : 'json';
            return $this->responseFactory->create($format)->getResponse($response, $this->anisQueryBuilder);
        } catch (SearchException $e) {
            throw new HttpBadRequestException(
                $request,
                $e->getMessage()
            );
        }
    }
}
