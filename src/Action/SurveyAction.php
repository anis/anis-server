<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Database;
use App\Entity\Survey;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class SurveyAction extends AbstractAction
{
    /**
     * `GET` Returns the survey found
     * `PUT` Full update the survey and returns the new version
     * `DELETE` Delete the survey found and return a confirmation message
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct survey with primary key
        $survey = $this->em->find('App\Entity\Survey', $args['name']);

        // If survey is not found 404
        if (is_null($survey)) {
            throw new HttpNotFoundException(
                $request,
                'Survey with name ' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($survey, JSON_UNESCAPED_SLASHES);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            foreach (array('label', 'description', 'link', 'manager', 'id_database') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the survey'
                    );
                }
            }

            // Database exists ?
            $idDatabase = $parsedBody['id_database'];
            $database = $this->em->find('App\Entity\Database', $idDatabase);
            if (is_null($database)) {
                throw new HttpBadRequestException(
                    $request,
                    'Database with id ' . $idDatabase . ' is not found'
                );
            }

            $this->editSurvey($survey, $parsedBody, $database);
            $payload = json_encode($survey, JSON_UNESCAPED_SLASHES);
        }

        if ($request->getMethod() === DELETE) {
            $name = $survey->getName();
            $this->em->remove($survey);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Survey ' . $name . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update survey object with setters
     *
     * @param Survey  $survey      The survey to update
     * @param string[] $parsedBody Contains the new values ​​of the survey sent by the user
     * @param Database $database   Contains the database doctrine object to set to the survey
     */
    private function editSurvey(Survey $survey, array $parsedBody, Database $database): void
    {
        $survey->setLabel($parsedBody['label']);
        $survey->setDescription($parsedBody['description']);
        $survey->setLink($parsedBody['link']);
        $survey->setManager($parsedBody['manager']);
        $survey->setDatabase($database);
        $this->em->flush();
    }
}
