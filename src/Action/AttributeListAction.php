<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Entity\Dataset;
use App\Entity\Attribute;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class AttributeListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all attributes of a dataset listed in the metamodel database
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $dataset = $this->em->find('App\Entity\Dataset', $args['name']);

        // Returns HTTP 404 if the dataset is not found
        if (is_null($dataset)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset with name ' . $args['name'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $attributes = $this->em->getRepository('App\Entity\Attribute')->findBy(
                array('dataset' => $dataset),
                array('id' => 'ASC')
            );
            $payload = json_encode($attributes);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            $fields = array(
                'id',
                'name',
                'label',
                'form_label',
                'type',
                'criteria_display',
                'output_display',
                'display_detail',
                'order_display'
            );
            foreach ($fields as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new attribute'
                    );
                }
            }

            $attribute = $this->postAttribute($parsedBody, $dataset);
            $payload = json_encode($attribute);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Add a new attribute into the metamodel
     *
     * @param array $parsedBody Contains the values ​​of the new attribute sent by the user
     * @param Dataset $dataset The attribute's dataset
     *
     * @return Attribute
     */
    private function postAttribute(array $parsedBody, Dataset $dataset): Attribute
    {
        $attribute = new Attribute($this->getValue($parsedBody, 'id'), $dataset);
        $attribute->setName($this->getValue($parsedBody, 'name'));
        $attribute->setLabel($this->getValue($parsedBody, 'label'));
        $attribute->setFormLabel($this->getValue($parsedBody, 'form_label'));
        $attribute->setDescription($this->getValue($parsedBody, 'description'));
        $attribute->setOutputDisplay($this->getValue($parsedBody, 'output_display'));
        $attribute->setCriteriaDisplay($this->getValue($parsedBody, 'criteria_display'));
        $attribute->setSearchFlag($this->getValue($parsedBody, 'search_flag'));
        $attribute->setSearchType($this->getValue($parsedBody, 'search_type'));
        $attribute->setOperator($this->getValue($parsedBody, 'operator'));
        $attribute->setType($this->getValue($parsedBody, 'type'));
        $attribute->setMin($this->getValue($parsedBody, 'min'));
        $attribute->setMax($this->getValue($parsedBody, 'max'));
        $attribute->setPlaceholderMin($this->getValue($parsedBody, 'placeholder_min'));
        $attribute->setPlaceholderMax($this->getValue($parsedBody, 'placeholder_max'));
        $attribute->setRenderer($this->getValue($parsedBody, 'renderer'));
        $attribute->setRendererConfig($this->getValue($parsedBody, 'renderer_config'));
        $attribute->setDisplayDetail($this->getValue($parsedBody, 'display_detail'));
        if (!is_null($this->getValue($parsedBody, 'selected'))) {
            $attribute->setSelected($this->getValue($parsedBody, 'selected'));
        }
        if (!is_null($this->getValue($parsedBody, 'order_by'))) {
            $attribute->setOrderBy($this->getValue($parsedBody, 'order_by'));
        }
        $attribute->setOrderDisplay($this->getValue($parsedBody, 'order_display'));
        if (!is_null($this->getValue($parsedBody, 'detail'))) {
            $attribute->setDetail($this->getValue($parsedBody, 'detail'));
        }
        $attribute->setRendererDetail($this->getValue($parsedBody, 'renderer_detail'));
        $attribute->setRendererDetailConfig($this->getValue($parsedBody, 'renderer_detail_config'));
        $attribute->setOptions($this->getValue($parsedBody, 'options'));
        $attribute->setVoUtype($this->getValue($parsedBody, 'vo_utype'));
        $attribute->setVoUcd($this->getValue($parsedBody, 'vo_ucd'));
        $attribute->setVoUnit($this->getValue($parsedBody, 'vo_unit'));
        $attribute->setVoDescription($this->getValue($parsedBody, 'vo_description'));
        $attribute->setVoDatatype($this->getValue($parsedBody, 'vo_datatype'));
        $attribute->setVoSize($this->getValue($parsedBody, 'vo_size'));
        if (is_null($this->getValue($parsedBody, 'id_criteria_family'))) {
            $criteriaFamily = null;
        } else {
            $criteriaFamily = $this->em->find(
                'App\Entity\CriteriaFamily',
                $parsedBody['id_criteria_family']
            );
        }
        $attribute->setCriteriaFamily($criteriaFamily);
        if (is_null($this->getValue($parsedBody, 'id_output_category'))) {
            $outputCategory = null;
        } else {
            $outputCategory = $this->em->find(
                'App\Entity\OutputCategory',
                $parsedBody['id_output_category']
            );
        }
        $attribute->setOutputCategory($outputCategory);

        $this->em->persist($attribute);
        $this->em->flush();

        return $attribute;
    }

    private function getValue($parsedBody, $key)
    {
        if (array_key_exists($key, $parsedBody)) {
            return $parsedBody[$key];
        } else {
            return null;
        }
    }
}
