<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Entity\Option;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class OptionAction extends AbstractAction
{
    /**
     * `GET`    Returns one option by ID
     * `PUT`    Edit one option
     * `DELETE` Delete one option
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct option with primary key (ID)
        $option = $this->em->find('App\Entity\Option', $args['id']);

        // If option is not found 404
        if (is_null($option)) {
            throw new HttpNotFoundException(
                $request,
                'Option with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($option);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            foreach (array('label', 'value', 'display') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to edit the option'
                    );
                }
            }

            $this->editOption($option, $parsedBody);
            $payload = json_encode($option);
        }

        if ($request->getMethod() === DELETE) {
            $id = $option->getId();
            $this->em->remove($option);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Option with id ' . $id . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
    * Update option object with setters
    *
    * @param Option $option     The option object to update
    * @param array  $parsedBody Contains the new values ​​of the option sent by the user
    */
    private function editOption(Option $option, array $parsedBody): void
    {
        $option->setLabel($parsedBody['label']);
        $option->setValue($parsedBody['value']);
        $option->setDisplay($parsedBody['display']);
        $this->em->flush();
    }
}
