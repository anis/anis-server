<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use App\Entity\Select;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class SelectListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all select listed in the metamodel database
     * `POST` Add a new select
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        if ($request->getMethod() === GET) {
            $selectList = $this->em->getRepository('App\Entity\Select')->findAll();
            $payload = json_encode($selectList);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // To work this action needs user information to update
            foreach (array('name', 'label') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new select'
                    );
                }
            }

            $select = $this->postSelect($parsedBody);
            $payload = json_encode($select);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Add a new select into the settings database
     *
     * @param array $parsedBody Contains the values ​​of the new select
     */
    private function postSelect(array $parsedBody): Select
    {
        $select = new Select($parsedBody['name']);
        $select->setLabel($parsedBody['label']);

        $this->em->persist($select);
        $this->em->flush();

        return $select;
    }
}
