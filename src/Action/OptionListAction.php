<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Entity\Select;
use App\Entity\Option;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class OptionListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all option available for one select
     * `POST` Add a new option for a select
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        if ($request->getMethod() === GET) {
            $optionList = $this->em->getRepository('App\Entity\Option')->findAll();
            $payload = json_encode($optionList);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // To work this action needs user information to update
            foreach (array('label', 'value', 'display', 'select_name') as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new option'
                    );
                }
            }

            // Search the correct select with primary key (name)
            $selectName = $parsedBody['select_name'];
            $select = $this->em->find('App\Entity\Select', $selectName);

            // If select is not found 404
            if (is_null($select)) {
                throw new HttpNotFoundException(
                    $request,
                    'Select with name ' . $selectName . ' is not found'
                );
            }

            $option = $this->postOption($select, $parsedBody);
            $payload = json_encode($option);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * @param Select $select     Select on which the option is added
     * @param array  $parsedBody Contains the values ​​of the new option sent by the user
     *
     * @return Option
     */
    private function postOption(Select $select, array $parsedBody): Option
    {
        $option = new Option($select);
        $option->setLabel($parsedBody['label']);
        $option->setValue($parsedBody['value']);
        $option->setDisplay($parsedBody['display']);

        $this->em->persist($option);
        $this->em->flush();

        return $option;
    }
}
