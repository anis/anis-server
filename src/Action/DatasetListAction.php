<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Survey;
use App\Entity\DatasetFamily;
use App\Entity\Dataset;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class DatasetListAction extends AbstractAction
{
    /**
     * `GET`  Returns a list of all datasets for a given dataset family
     * `POST` Add a new dataset
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $datasetFamily = $this->em->find('App\Entity\DatasetFamily', $args['id']);

        // Returns HTTP 404 if the dataset family is not found
        if (is_null($datasetFamily)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset family with id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $datasets = $this->em->getRepository('App\Entity\Dataset')->findBy(
                array('datasetFamily' => $datasetFamily)
            );
            $payload = json_encode($datasets);
        }

        if ($request->getMethod() === POST) {
            $parsedBody = $request->getParsedBody();

            // If mandatories empty fields 400
            $fields = array(
                'name',
                'table_ref',
                'label',
                'description',
                'display',
                'data_path',
                'config',
                'public',
                'survey_name'
            );
            foreach ($fields as $a) {
                if ($this->isEmptyField($a, $parsedBody)) {
                    throw new HttpBadRequestException(
                        $request,
                        'Param ' . $a . ' needed to add a new dataset'
                    );
                }
            }

            // Survey is mandatory to add a new dataset
            $surveyName = $parsedBody['survey_name'];
            $survey = $this->em->find('App\Entity\Survey', $surveyName);
            if (is_null($survey)) {
                throw new HttpBadRequestException(
                    $request,
                    'Survey with name ' . $surveyName . ' is not found'
                );
            }

            $dataset = $this->postDataset($parsedBody, $survey, $datasetFamily);
            $payload = json_encode($dataset);
            $response = $response->withStatus(201);
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Create a new dataset doctrine object and save it
     *
     * @param array         $parsedBody    Contains the values ​​of the new dataset sent by the user
     * @param Survey        $survey        Contains the survey doctrine object
     * @param DatasetFamily $datasetFamily Contains the dataset family doctrine object
     *
     * @return Dataset
     */
    private function postDataset(
        array $parsedBody,
        Survey $survey,
        DatasetFamily $datasetFamily
    ): Dataset {
        $dataset = new Dataset($parsedBody['name']);
        $dataset->setTableRef($parsedBody['table_ref']);
        $dataset->setLabel($parsedBody['label']);
        $dataset->setDescription($parsedBody['description']);
        $dataset->setDisplay($parsedBody['display']);
        $dataset->setDataPath($parsedBody['data_path']);
        $dataset->setConfig($parsedBody['config']);
        $dataset->setPublic($parsedBody['public']);
        $dataset->setSurvey($survey);
        $dataset->setDatasetFamily($datasetFamily);

        $this->em->persist($dataset);
        $this->em->flush();

        return $dataset;
    }
}
