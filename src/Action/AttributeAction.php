<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use App\Entity\Attribute;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class AttributeAction extends AbstractAction
{
    /**
     * `GET` Returns the attribute found
     * `PUT` Full update the attribute and returns the new version
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        $attribute = $this->em->getRepository('App\Entity\Attribute')->findOneBy(
            array('dataset' => $args['name'], 'id' => $args['id'])
        );

        // If attribute is not found 404
        if (is_null($attribute)) {
            throw new HttpNotFoundException(
                $request,
                'Attribute with dataset name ' . $args['name'] . ' and attribute id ' . $args['id'] . ' is not found'
            );
        }

        if ($request->getMethod() === GET) {
            $payload = json_encode($attribute);
        }

        if ($request->getMethod() === PUT) {
            $parsedBody = $request->getParsedBody();
            $this->editAttribute($attribute, $parsedBody);
            $payload = json_encode($attribute);
        }

        if ($request->getMethod() === DELETE) {
            $id = $attribute->getId();
            $this->em->remove($attribute);
            $this->em->flush();
            $payload = json_encode(array('message' => 'Attribute with id ' . $id . ' is removed!'));
        }

        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Update attribute object with setters
     *
     * @param Attribute $attribute  The attribute to update
     * @param array     $parsedBody Contains the new values ​​of the attribute sent by the user
     */
    private function editAttribute(Attribute $attribute, array $parsedBody): void
    {
        $attribute->setId($parsedBody['id']);
        $attribute->setName($parsedBody['name']);
        $attribute->setLabel($parsedBody['label']);
        $attribute->setFormLabel($parsedBody['form_label']);
        $attribute->setDescription($parsedBody['description']);
        $attribute->setOutputDisplay($parsedBody['output_display']);
        $attribute->setCriteriaDisplay($parsedBody['criteria_display']);
        $attribute->setSearchFlag($parsedBody['search_flag']);
        $attribute->setSearchType($parsedBody['search_type']);
        $attribute->setOperator($parsedBody['operator']);
        $attribute->setType($parsedBody['type']);
        $attribute->setMin($parsedBody['min']);
        $attribute->setMax($parsedBody['max']);
        $attribute->setPlaceholderMin($parsedBody['placeholder_min']);
        $attribute->setPlaceholderMax($parsedBody['placeholder_max']);
        $attribute->setRenderer($parsedBody['renderer']);
        $attribute->setRendererConfig($parsedBody['renderer_config']);
        $attribute->setDisplayDetail($parsedBody['display_detail']);
        $attribute->setSelected($parsedBody['selected']);
        $attribute->setOrderBy($parsedBody['order_by']);
        $attribute->setOrderDisplay($parsedBody['order_display']);
        $attribute->setDetail($parsedBody['detail']);
        $attribute->setRendererDetail($parsedBody['renderer_detail']);
        $attribute->setRendererDetailConfig($parsedBody['renderer_detail_config']);
        $attribute->setOptions($parsedBody['options']);
        $attribute->setVoUtype($parsedBody['vo_utype']);
        $attribute->setVoUcd($parsedBody['vo_ucd']);
        $attribute->setVoUnit($parsedBody['vo_unit']);
        $attribute->setVoDescription($parsedBody['vo_description']);
        $attribute->setVoDatatype($parsedBody['vo_datatype']);
        $attribute->setVoSize($parsedBody['vo_size']);
        if (is_null($parsedBody['id_criteria_family'])) {
            $criteriaFamily = null;
        } else {
            $criteriaFamily = $this->em->find(
                'App\Entity\CriteriaFamily',
                $parsedBody['id_criteria_family']
            );
        }
        $attribute->setCriteriaFamily($criteriaFamily);
        if (is_null($parsedBody['id_output_category'])) {
            $outputCategory = null;
        } else {
            $outputCategory = $this->em->find(
                'App\Entity\OutputCategory',
                $parsedBody['id_output_category']
            );
        }
        $attribute->setOutputCategory($outputCategory);
        $this->em->flush();
    }
}
