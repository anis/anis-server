<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpInternalServerErrorException;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Factory\Psr17Factory;
use App\Search\DBALConnectionFactory;
use App\Search\Query\AnisQueryBuilder;
use App\Search\Response\IResponseFactory;
use App\Search\SearchException;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class ArchiveAction extends AbstractAction
{
    /**
     * Contains anis-server data path
     *
     * @var string
     */
    private $dataPath;

    /**
     * @var DBALConnectionFactory
     */
    private $connectionFactory;

    /**
     * @var AnisQueryBuilder
     */
    private $anisQueryBuilder;

    /**
     * Contains settings to handle Json Web Token (app/settings.php)
     *
     * @var array
     */
    private $settings;

    /**
     * Create the classe before call __invoke to execute the action
     *
     * @param EntityManagerInterface $em Doctrine       Entity Manager Interface
     * @param DBALConnectionFactory  $connectionFactory Factory used to construct connection to business database
     * @param AnisQueryBuilder       $anisQueryBuilder  Object used to wrap the Doctrine DBAL Query Builder
     * @param IResponseFactory       $responseFactory   Contains the factory used to return formatted response
     * @param array                  $settings          Settings about token
     */
    public function __construct(
        EntityManagerInterface $em,
        string $dataPath,
        DBALConnectionFactory $connectionFactory,
        AnisQueryBuilder $anisQueryBuilder,
        array $settings
    ) {
        parent::__construct($em);
        $this->dataPath = $dataPath;
        $this->connectionFactory = $connectionFactory;
        $this->anisQueryBuilder = $anisQueryBuilder;
        $this->settings = $settings;
    }

    /**
     * `GET` Returns the file found
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct dataset with primary key
        $datasetName = $args['dname'];
        $dataset = $this->em->find('App\Entity\Dataset', $datasetName);

        // If dataset is not found 404
        if (is_null($dataset)) {
            throw new HttpNotFoundException(
                $request,
                'Dataset with name ' . $datasetName . ' is not found'
            );
        }

        // If dataset is private and authorization enabled
        if (!$dataset->getPublic() && boolval($this->settings['enabled'])) {
            $this->verifyDatasetAuthorization($request, $dataset->getName(), $this->settings['admin_role']);
        }

        $queryParams = $request->getQueryParams();

        // The parameter "a" is mandatory
        if (!array_key_exists('a', $queryParams)) {
            throw new HttpBadRequestException(
                $request,
                'Param a is required for this request'
            );
        }

        try {
            // Configure the Anis Query Builder
            $connection = $this->connectionFactory->create($dataset->getSurvey()->getDatabase());
            $this->anisQueryBuilder->setDoctrineQueryBuilder($connection->createQueryBuilder());
            $this->anisQueryBuilder->setDatasetSelected($dataset);

            // Build the SQL request
            $this->anisQueryBuilder->build($queryParams);
        } catch (SearchException $e) {
            throw new HttpBadRequestException(
                $request,
                $e->getMessage()
            );
        }

        $zipFile = '/tmp/archive_' . $dataset->getName() . '_' . (new \DateTime())->format('Y-m-d\TH:i:s') . '.zip';
        $zip = new \ZipArchive();

        if ($zip->open($zipFile, \ZipArchive::CREATE) !== true) {
            throw new HttpInternalServerErrorException(
                $request,
                'Unable to open the file ' . $zipFile
            );
        }

        // Attributes with search_flag = File
        $attributesSelected = $this->anisQueryBuilder->getAttributesSelected();
        $stmt = $this->anisQueryBuilder->getDoctrineQueryBuilder()->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            foreach ($attributesSelected as $attribute) {
                $attributeLabel = $attribute->getLabel();
                $filePath = $this->dataPath . $dataset->getDataPath() . DIRECTORY_SEPARATOR . $row[$attributeLabel];
                if (file_exists($filePath)) {
                    $zip->addFile($filePath, $row[$attributeLabel]);
                }
            }
        }

        $zip->close();

        // Stream created ZIP file
        $psr17Factory = new Psr17Factory();
        $stream = $psr17Factory->createStreamFromFile($zipFile, 'r');

        return $response->withBody($stream)
            ->withHeader('Content-Disposition', 'attachment; filename=' . basename($zipFile) . ';')
            ->withHeader('Content-Type', mime_content_type($zipFile))
            ->withHeader('Content-Length', filesize($zipFile));
    }
}
