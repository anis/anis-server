<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class AdminFileExplorerAction
{
    /**
     * Contains anis-server data path
     *
     * @var string
     */
    private $dataPath;

    /**
     * Create the classe before call __invoke to execute the action
     *
     * @param string $dataPath Contains anis-server data path
     */
    public function __construct(string $dataPath)
    {
        $this->dataPath = $dataPath;
    }

    /**
     * `GET` Returns the list of files in the directory
     *
     * @param  ServerRequestInterface $request  PSR-7 This object represents the HTTP request
     * @param  ResponseInterface      $response PSR-7 This object represents the HTTP response
     * @param  string[]               $args     This table contains information transmitted in the URL (see routes.php)
     *
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if ($request->getMethod() === OPTIONS) {
            return $response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        $path = $this->dataPath;

        if (array_key_exists('fpath', $args)) {
            $path .= DIRECTORY_SEPARATOR . $args['fpath'];
        }

        if (is_file($path)) {
            throw new HttpBadRequestException(
                $request,
                'The requested path is a file'
            );
        }

        if (!file_exists($path)) {
            throw new HttpNotFoundException(
                $request,
                'The requested path is not found'
            );
        }

        $files = array();

        foreach (scandir($path) as $file) {
            $files[] = array(
                'name' => $file,
                'size' => filesize($path . DIRECTORY_SEPARATOR . $file),
                'type' => filetype($path . DIRECTORY_SEPARATOR . $file),
                'mimetype' => mime_content_type($path . DIRECTORY_SEPARATOR . $file)
            );
        }

        $response->getBody()->write(json_encode($files));
        return $response;
    }
}
