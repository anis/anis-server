<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Nyholm\Psr7\Response as NyholmResponse;
use Psr\Http\Server\MiddlewareInterface;
use Firebase\JWT\JWT;

/**
 * Middleware to handle Authorization request header (JWT)
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Middleware
 */
final class AuthorizationMiddleware implements MiddlewareInterface
{
    /**
     * Contains settings to handle Json Web Token
     *
     * @var array
     */
    private $settings;

    /**
     * Create the classe before call process to execute this middleware
     *
     * @param array $settings Settings about token
     */
    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * Try to validating and verifing the signature of the json web token
     * if Authorization header is present
     *
     * @param  ServerRequest  $request PSR-7 request
     * @param  RequestHandler $handler PSR-15 request handler
     *
     * @return Response
     */
    public function process(Request $request, RequestHandler $handler): Response
    {
        if (
            $request->getMethod() === OPTIONS
            || !$request->hasHeader('Authorization')
            || !boolval($this->settings['enabled'])
        ) {
            return $handler->handle($request);
        }

        // Get token string from Authorizarion header
        $bearer = $request->getHeader('Authorization');
        $data = explode(' ', $bearer[0]);
        if ($data[0] !== 'Bearer') {
            return $this->getUnauthorizedResponse(
                'HTTP 401: Authorization must contain a string with the following format -> Bearer JWT'
            );
        }

        // Read public key
        $publicKey = file_get_contents($this->settings['public_key_file']);

        try {
            $token = JWT::decode($data[1], $publicKey, array('RS256'));
        } catch (\Exception $e) {
            return $this->getUnauthorizedResponse('HTTP 401: ' . $e->getMessage());
        }

        return $handler->handle($request->withAttribute('token', $token));
    }

    /**
     * @param string $message Unauthorized response message
     *
     * @return Response
     */
    private function getUnauthorizedResponse(string $message): Response
    {
        $resonse = new NyholmResponse();
        $resonse->getBody()->write(json_encode(array(
            'message' => $message
        )));
        return $resonse->withStatus(401);
    }
}
