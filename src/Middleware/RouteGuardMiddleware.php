<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Http\Server\MiddlewareInterface;
use Nyholm\Psr7\Response as NyholmResponse;

/**
 * Middleware to protected a group of routes (Authorization required)
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Middleware
 */
final class RouteGuardMiddleware implements MiddlewareInterface
{
    /**
     * True if authorization is activated false else
     *
     * @var bool
     */
    private $authorizationEnabled;

    /**
     * List the HTTP methods to be protected for the requested route
     *
     * @var array
     */
    private $methods;

    /**
     * Role to which the user must belong to past the guard
     *
     * @var string
     */
    private $role;

    /**
     * Create the classe before call process to execute this middleware
     *
     * @param array  $authorizationEnabled True if authorization is activated false else
     * @param bool   $methods              List the HTTP methods to be protected for the requested route
     * @param string $role                 Role to which the user must belong to past the guard
     */
    public function __construct(bool $authorizationEnabled, array $methods, string $role)
    {
        $this->authorizationEnabled = $authorizationEnabled;
        $this->methods = $methods;
        $this->role = $role;
    }

    /**
     * Verify authorization
     *
     * @param ServerRequest  $request PSR-7 request
     * @param RequestHandler $handler PSR-15 request handler
     *
     * @return Response
     */
    public function process(Request $request, RequestHandler $handler): Response
    {
        if ($request->getMethod() === OPTIONS || !$this->authorizationEnabled) {
            return $handler->handle($request);
        }

        if (!in_array($request->getMethod(), $this->methods)) {
            return $handler->handle($request);
        }

        $token = $request->getAttribute('token');
        if (!$token) {
            return $this->getResponse('HTTP 401: This url need a valid token', 401);
        }
        if (!in_array($this->role, $token->realm_access->roles)) {
            return $this->getResponse('HTTP 403: This url need a higher level of permission', 403);
        }

        return $handler->handle($request);
    }

    /**
     * @param string $message Unauthorized response message
     * @param int    $code    HTTP code returns
     *
     * @return Response
     */
    private function getResponse(string $message, int $code): Response
    {
        $resonse = new NyholmResponse();
        $resonse->getBody()->write(json_encode(array(
            'message' => $message
        )));
        return $resonse->withStatus($code);
    }
}
