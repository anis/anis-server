# Anis Server 

[![pipeline status](https://gitlab.lam.fr/anis/anis-server/badges/develop/pipeline.svg)](https://gitlab.lam.fr/anis/anis-server/-/commits/develop) [![coverage report](https://gitlab.lam.fr/anis/anis-server/badges/develop/coverage.svg)](https://gitlab.lam.fr/anis/anis-server/-/commits/develop)

## Introduction

AstroNomical Information System is a generic web tool that aims to facilitate the provision of data (Astrophysics), accessible from a database, for the scientific community.

This software allows you to control one or more databases related to astronomical projects and allows access to datasets via URLs.

This repository is the `anis-server` sub-project. It offers a web API to control the metadata database and to search and retrieve astronomical data.

Anis is protected by the CeCILL licence (see LICENCE file at the software root).

## Authors

Here is the list of people involved in the development:

* `François Agneray` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `Chrystel Moreau` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `Tifenn Guillas` : Laboratoire d'Astrophysique de Marseille (CNRS)

## More resources:

* [Website](https://anis.lam.fr)
* [Documentation](https://anis.lam.fr/doc/)

## Installing and starting the application

Anis Server contains a Makefile that helps the developer to install and start the application.

To list all operations availables just type `make` in your terminal at the root of this application.

- To install all dependancies: `make install`
- To ebuild all images and start containers: `make rebuild`
- To start/stop/restart/status all services: `make start|stop|restart|status`
- To display logs for all services: `make logs`
- To open a shell command into php container: `make shell`
- To execute tests suite: `make phpunit`
- To execute php code sniffer: `make phpcs`
- To create the metamodel database: `make create-db`
- TO remove the metadata database: `make remove-pgdata`

## Production mode

To activate the production mode with no error details and doctrine cache, you must configure the following environment variables :

```
DISPLAY_ERROR_DETAILS: "false"
DATABASE_DEV_MODE: 0
```

## Few examples with curl

* To list all datasets available in the default instance => http://localhost:8080/instance/default/dataset
* To print all data for the obs_cat dataset with column 1, 2 and 3 => http://localhost:8080/search/observations?a=1;2;3
* To count the number of data available for a request => http://localhost:8080/search/observations?a=count
* To print only 3 obs_cat data (search by id) => http://localhost:8080/search/observations?a=1;2;3&c=1::in::418|419|420